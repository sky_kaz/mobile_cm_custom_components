part of '../cm_components.dart';

class CMTabBar extends StatelessWidget {
  final List<Tab> tabs;
  final TabController tabController;
  CMTabBar({@required this.tabs, this.tabController}) : assert(tabs != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(height: 40),
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
          color: CMColors.tabBarBackground,
          borderRadius: BorderRadius.all(Radius.circular(30))),
      child: TabBar(
        controller: tabController,
        isScrollable: tabs.length >= 4,
        labelPadding:
            EdgeInsets.symmetric(horizontal: tabs.length >= 4 ? 20 : 1),
        tabs: tabs,
        indicatorSize: TabBarIndicatorSize.tab,
        labelStyle: CMTextStyle.tabBarLabelStyle,
        unselectedLabelColor: CMColors.backgroundPurpleLite,
        labelColor: CMColors.backgroundWhite,
        unselectedLabelStyle: CMTextStyle.tabBarUnselectedLabelStyle,
        indicator: BubbleTabIndicator(
          insets: EdgeInsets.zero,
          indicatorRadius: 30,
          indicatorHeight: 34.0,
          indicatorColor: CMColors.tabBarIndicator,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
        ),
      ),
    );
  }
}
