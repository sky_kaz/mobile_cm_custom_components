part of '../cm_components.dart';

class CMCheckbox extends StatelessWidget {
  final ValueChanged<bool> onChanged;
  final bool isChecked;
  final BorderRadius borderRadius;
  CMCheckbox(
      {@required this.isChecked, @required this.onChanged, this.borderRadius})
      : assert(isChecked != null),
        assert(onChanged != null);
  final Duration animationDuration = const Duration(milliseconds: 250);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: borderRadius,
      onTap: () => onChanged(!isChecked),
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: AnimatedContainer(
          duration: animationDuration,
          width: 20,
          height: 20,
          decoration: BoxDecoration(
              borderRadius: borderRadius,
              border: Border.all(
                color: isChecked
                    ? CMColors.checkboxSelected
                    : CMColors.checkboxUnselected,
                width: 2,
              )),
          alignment: Alignment.center,
          child: AnimatedSwitcher(
            transitionBuilder: (child, animation) {
              return ScaleTransition(
                alignment: Alignment.center,
                scale: animation,
                child: child,
              );
            },
            duration: animationDuration,
            child: isChecked
                ? Icon(
                    //TODO проверить размер иконки
                    FontAwesomeIcons.check,
                    color: CMColors.checkboxSelected,
                    size: 11,
                  )
                : Container(),
          ),
        ),
      ),
    );
  }
}
