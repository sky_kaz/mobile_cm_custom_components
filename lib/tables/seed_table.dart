part of '../cm_components.dart';

class CMSeedTable extends StatelessWidget {
  final List<Widget> seeds;
  CMSeedTable({@required this.seeds}) : assert(seeds != null);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 205,
      width: 350,
      child: GridView.count(
        padding: EdgeInsets.zero,
        physics: NeverScrollableScrollPhysics(),
        childAspectRatio: 100 / 40,
        primary: false,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 3,
        children: seeds,
      ),
    );
  }
}
