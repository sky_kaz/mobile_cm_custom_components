part of '../cm_components.dart';

class CMExpandedText extends StatelessWidget {
  final Widget leftText;
  final Widget rightText;
  CMExpandedText({@required this.leftText, @required this.rightText})
      : assert(leftText != null),
        assert(rightText != null);
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: ConstrainedBox(
              constraints: BoxConstraints(minWidth: constraints.maxWidth),
              child: IntrinsicWidth(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    leftText,
                    Expanded(
                        child: SizedBox(
                      width: 10,
                      child: CustomPaint(
                        painter: DashedLinePainter(),
                      ),
                    )),
                    rightText
                  ],
                ),
              )));
    });
  }
}

class DashedLinePainter extends CustomPainter {
  final double dashWidth;
  final double dashSpace;
  final double strokeWidth;
  DashedLinePainter(
      {this.dashWidth = 2, this.dashSpace = 2, this.strokeWidth = 1});
  @override
  void paint(Canvas canvas, Size size) {
    double startX = 0;
    final paint = Paint()
      ..color = CMColors.dashedUnderline
      ..style = PaintingStyle.stroke
      ..strokeWidth = strokeWidth;
    while (startX < size.width) {
      canvas.drawLine(Offset(startX, -(strokeWidth * 2)),
          Offset(startX + dashWidth, -(strokeWidth * 2)), paint);
      startX += dashWidth + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
