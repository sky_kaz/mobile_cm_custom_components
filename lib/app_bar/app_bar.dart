part of '../cm_components.dart';

class CMAppBar extends PreferredSize {
  final Color color;
  final double height;
  final Widget leading;
  final Widget title;
  final List<Widget> actions;
  final Widget bottom;
  final EdgeInsets leadingPadding;
  final EdgeInsets titlePadding;
  final EdgeInsets actionsPadding;
  final iconTheme;
  final automaticallyImplyLeading;
  final void Function() onBackPressed;

  CMAppBar({
    @required this.height,
    this.color = Colors.transparent,
    this.automaticallyImplyLeading = true,
    this.title,
    this.bottom,
    this.leading,
    this.actions,
    this.actionsPadding = const EdgeInsets.only(right: 16),
    this.iconTheme = const IconThemeData(color: CMColors.backgroundWhite),
    this.leadingPadding = const EdgeInsets.only(left: 16, bottom: 5),
    this.titlePadding = const EdgeInsets.only(left: 24, right: 12),
    this.onBackPressed,
  }) : assert(height != null);
  CMAppBar.high({
    this.color = Colors.transparent,
    this.automaticallyImplyLeading = true,
    this.title,
    this.bottom,
    this.leading,
    this.actions,
    this.actionsPadding = const EdgeInsets.only(right: 16),
    this.iconTheme = const IconThemeData(color: CMColors.backgroundWhite),
    this.leadingPadding = const EdgeInsets.only(left: 16, bottom: 5),
    this.titlePadding = const EdgeInsets.only(left: 24, right: 12),
    this.onBackPressed,
  }) : height = 98;
  CMAppBar.small({
    this.color = Colors.transparent,
    this.automaticallyImplyLeading = true,
    this.title,
    this.bottom,
    this.leading,
    this.actions,
    this.actionsPadding = const EdgeInsets.only(right: 16),
    this.iconTheme = const IconThemeData(color: CMColors.backgroundWhite),
    this.leadingPadding = const EdgeInsets.only(left: 16, bottom: 5),
    this.titlePadding = const EdgeInsets.only(left: 24, right: 12),
    this.onBackPressed,
  }) : height = 87;

  @override
  Size get preferredSize => Size.fromHeight(height);
  @override
  Widget build(BuildContext context) {
    final ModalRoute<dynamic> parentRoute = ModalRoute.of(context);
    final bool canPop = parentRoute?.canPop ?? false;
    final double topPadding = MediaQuery.of(context).padding.top;

    return Container(
      height: preferredSize.height + (topPadding > 0 ? topPadding / 2 : 0),
      color: color,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _buildLeading(
                  leading, automaticallyImplyLeading, canPop, onBackPressed),
              Expanded(
                child: Padding(
                  padding: titlePadding,
                  child: title,
                ),
              ),
              Padding(
                padding: actionsPadding,
                child: Row(
                  children: actions ??
                      [
                        /// TODO: надо использовать более легкий виджет `const SizedBox();`
                        Container(
                          width: 27,
                          height: 27,
                        )
                      ],
                ),
              )
            ],
          ),
          if (bottom != null) bottom
        ],
      ),
    );
  }

  Widget _buildLeading(
      Widget leading, bool automaticallyImplyLeading, bool canPop,
      [void Function() onPressed]) {
    Widget _leading;
    if (leading == null && automaticallyImplyLeading && canPop) {
      _leading = CMAppBarLeading(onPressed);
    } else if (leading != null) {
      _leading = leading;
    }

    return Padding(
      padding: leadingPadding,
      child: _leading,
    );
  }
}

class CMAppBarLeading extends StatelessWidget {
  final void Function() onPressed;
  final Color leadingColor;
  CMAppBarLeading([this.onPressed,this.leadingColor = CMColors.appBarLeading]);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(width: 27, height: 27),
      child: IconButton(
        padding: EdgeInsets.zero,
        icon: Icon(
          CMIcons.back,
          color: leadingColor,
        ),
        iconSize: 33,
        onPressed: () => this.onPressed != null
            ? this.onPressed.call()
            : Navigator.maybePop(context),
      ),
    );
  }
}
