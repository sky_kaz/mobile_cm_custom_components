part of '../cm_components.dart';

class CMNodeListTile extends StatefulWidget {
  final List<Widget> tags;
  final String title;
  final ValueChanged<bool> onChoose;
  final bool isChoosed;
  final Duration duration;
  final EdgeInsets innerPadding;
  final Widget trailing;
  CMNodeListTile(
      {@required this.title,
      this.tags,
      this.innerPadding =
          const EdgeInsets.only(left: 15, top: 19, right: 6, bottom: 23),
      this.trailing,
      @required this.isChoosed,
      @required this.duration,
      @required this.onChoose})
      : assert(title != null),
        assert(duration != null),
        assert(isChoosed != null),
        assert(onChoose != null);
  @override
  _CMNodeListTileState createState() => _CMNodeListTileState();
}

class _CMNodeListTileState extends State<CMNodeListTile> {
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      //TODO проверить, нужен ли wrap и сколько строчек у текста
      // height: 93,
      padding: widget.innerPadding,
      duration: widget.duration,
      decoration: ShapeDecoration(
          shape: widget.isChoosed
              ? RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(6)))
              : UnderlineRoundedBorderWithPadding(
                  bottom: BorderSide(
                      width: 1, color: CMColors.underlineBorder),
                  underlineLeftPadding: widget.innerPadding.left,
                  underlineRightPadding: widget.innerPadding.right,
                  borderRadius: BorderRadius.all(Radius.circular(6))),
          color: widget.isChoosed ? CMColors.purpleAccent : Colors.transparent),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CMAnimatedCircle(
                      onTap: () => widget.onChoose(!widget.isChoosed),
                      isChoosed: widget.isChoosed,
                      duration: widget.duration,
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.only(left: 20),
                        child: AnimatedDefaultTextStyle(
                          duration: widget.duration,
                          style: widget.isChoosed
                              ? TextStyle(color: CMColors.backgroundWhite)
                              : TextStyle(color: CMColors.backgroundPurpleLite),
                          child: Text(
                            widget.title.toUpperCase(),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: CMTextStyle.nodeListTileTitleStyle,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                if (widget.tags?.isNotEmpty ?? false)
                  Padding(
                    padding: EdgeInsets.only(top: 12),
                    child: Wrap(
                      spacing: 7,
                      runSpacing: 12,
                      children: widget.tags,
                    ),
                  )
              ],
            ),
          ),
          //TODO нужен ли padding
          if (widget.trailing != null)
            Padding(
              padding: EdgeInsets.only(left: 6, top: 10),
              child: widget.trailing,
            )
        ],
      ),
    );
  }
}

//TODO круглый или квадратный? в дизайне по разному
class CMAnimatedCircle extends StatelessWidget {
  final bool isChoosed;
  final Duration duration;
  final VoidCallback onTap;
  CMAnimatedCircle(
      {@required this.isChoosed, @required this.duration, this.onTap})
      : assert(isChoosed != null),
        assert(duration != null);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      customBorder: CircleBorder(),
      onTap: onTap,
      child: AnimatedContainer(
        duration: duration,
        width: 20,
        height: 20,
        padding: EdgeInsets.all(3),
        decoration: BoxDecoration(
            border: Border.all(
                width: 2,
                color: isChoosed
                    ? CMColors.backgroundWhite
                    : CMColors.animatedCircleUnselected),
            shape: BoxShape.circle,
            color: isChoosed ? CMColors.purpleAccent : Colors.transparent),
        child: AnimatedContainer(
          duration: duration,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: isChoosed ? CMColors.backgroundWhite : Colors.transparent),
        ),
      ),
    );
  }
}

class UnderlineRoundedBorderWithPadding extends RoundedRectangleBorder {
  const UnderlineRoundedBorderWithPadding(
      {this.bottom = BorderSide.none,
      this.borderRadius = BorderRadius.zero,
      this.underlineRightPadding = 0,
      this.underlineLeftPadding = 0})
      : assert(bottom != null),
        assert(borderRadius != null);

  final BorderSide bottom;

  final BorderRadiusGeometry borderRadius;
  final double underlineLeftPadding;

  final double underlineRightPadding;

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {
    final Paint paint = Paint()..strokeWidth = 0.0;

    final Path path = Path();

    switch (bottom.style) {
      case BorderStyle.solid:
        paint.color = bottom.color;
        path.moveTo(rect.right - underlineRightPadding, rect.bottom);
        path.lineTo(rect.left + underlineLeftPadding, rect.bottom);
        if (bottom.width == 0.0) {
          paint.style = PaintingStyle.stroke;
        } else {
          paint.style = PaintingStyle.fill;
          path.lineTo(
              rect.left + underlineLeftPadding, rect.bottom - bottom.width);
          path.lineTo(
              rect.right - underlineRightPadding, rect.bottom - bottom.width);
        }
        canvas.drawPath(path, paint);
        break;
      case BorderStyle.none:
        break;
    }
  }
}
