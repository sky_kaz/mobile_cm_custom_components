part of '../cm_components.dart';

class CMAddAnAddressTile extends StatefulWidget {
  final VoidCallback onTileTap;
  final String title;
  final String subTitle;
  final String note;
  final String recommendTitle;
  final Color recommendColor;
  final String svgIconAsset;
  final String svgBackgroundAsset;

  CMAddAnAddressTile(
      {@required this.title,
      @required this.subTitle,
      this.note,
      this.recommendTitle,
      this.recommendColor,
      this.svgIconAsset,
      this.svgBackgroundAsset,
      @required this.onTileTap})
      : assert(title != null),
        assert(subTitle != null),
        assert(onTileTap != null);

  @override
  _CMAddAnAddressTileState createState() => _CMAddAnAddressTileState();
}

class _CMAddAnAddressTileState extends State<CMAddAnAddressTile> {
  static const Duration animationDuration = Duration(milliseconds: 250);
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      onTap: () {
        setState(() {
          isTapped = !isTapped;
        });
        Future.delayed(animationDuration, () {
          widget.onTileTap();
          setState(() {
            isTapped = !isTapped;
          });
        });
      },
      child: DottedBorder(
        color: CMColors.addAnAddressTileBorderColor,
        strokeWidth: !isTapped ? 1.0 : 0.0,
        borderType: BorderType.RRect,
        radius: Radius.circular(10),
        padding: EdgeInsets.zero,
        child: AnimatedContainer(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: isTapped
                  ? [
                      CMColors.addAnAddressTileBackgroundSelectedGradientLeft,
                      CMColors.addAnAddressTileBackgroundSelectedGradientRight,
                    ]
                  : [
                      Colors.transparent,
                      Colors.transparent,
                    ],
              tileMode: TileMode.repeated,
            ),
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          duration: animationDuration,
          child: Stack(
            children: [
              if (widget.svgBackgroundAsset?.isNotEmpty ?? false)
                ClipRRect(
                  borderRadius: BorderRadius.circular(10.0),
                  child: SvgPicture.asset(
                    "cm_components/assets/add_an_address_phrase.svg",
                    fit: BoxFit.none,
                    color: !isTapped ? null : CMColors.backgroundWhite,
                  ),
                ),
              AnimatedContainer(
                padding: EdgeInsets.symmetric(horizontal: 22.0, vertical: 26.0),
                duration: animationDuration,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Center(
                      child: RepaintBoundary(
                        //TODO: унифицировать ли размеры иконок, импортированных с дизайна?

                        child: widget.svgIconAsset != null
                            ? SvgPicture.asset(
                                widget.svgIconAsset,
                                //width: 80,
                                //height: 80,
                                color: !isTapped
                                    ? CMColors.purpleAccent
                                    : CMColors.backgroundWhite,
                              )
                            : Container(),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 22.0, vertical: 26.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AnimatedDefaultTextStyle(
                          style: isTapped
                              ? CMTextStyle.addAnAddressTileTitleSelected
                              : CMTextStyle.addAnAddressTileTitleUnselected,
                          duration: animationDuration,
                          child: Text(widget.title)),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: AnimatedDefaultTextStyle(
                            style: isTapped
                                ? CMTextStyle.addAnAddressTileSubtitleSelected
                                : CMTextStyle
                                    .addAnAddressTileSubtitleUnselected,
                            duration: animationDuration,
                            child: Text(widget.subTitle)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: AnimatedDefaultTextStyle(
                          style: isTapped
                              ? CMTextStyle.addAnAddressTileNoteSelected
                              : CMTextStyle.addAnAddressTileNoteUnselected,
                          duration: animationDuration,
                          child: Text(widget.note ?? ''),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: AnimatedDefaultTextStyle(
                          style: isTapped
                              ? CMTextStyle
                                  .addAnAddressTileRecommendTitleSelected
                              : CMTextStyle
                                  .addAnAddressTileRecommendTitleUnselected(
                                      widget.recommendColor ?? Colors.black),
                          duration: animationDuration,
                          child: Text(widget.recommendTitle ?? ''),
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
