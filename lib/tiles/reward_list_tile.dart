// part of '../cm_components.dart';

// enum RewardDateTypes {
//   last_week,
//   last_month,
//   last_3_months,
//   last_6_months,
//   year_to_date
// }

// class CMRewardListTile extends StatefulWidget {
//   final String amount;
//   final String currency;
//   final RewardDateTypes rewardDateType;
//   final BuildContext parentContext;
//   CMRewardListTile(
//       {@required this.amount,
//       @required this.currency,
//       @required this.rewardDateType,
//       this.parentContext})
//       : assert(amount != null),
//         assert(currency != null),
//         assert(rewardDateType != null);

//   @override
//   _CMRewardListTileState createState() => _CMRewardListTileState();
// }

// class _CMRewardListTileState extends State<CMRewardListTile>
//     with SingleTickerProviderStateMixin {
//   PopupSlideRoute _popupSlideRoute;
//   AnimationController _rotateController;
//   Animation<double> _iconTurns;
//   bool isPopupShown = false;
//   final Duration animationDuration = const Duration(milliseconds: 250);
//   @override
//   void initState() {
//     super.initState();
//     _rotateController =
//         AnimationController(duration: animationDuration, vsync: this);
//     _iconTurns = Tween<double>(begin: 0.0, end: 0.5).animate(
//       CurvedAnimation(
//         parent: _rotateController,
//         curve: Curves.easeIn,
//       ),
//     );
//   }

//   Rect _getRenderBoxData(BuildContext context) {
//     RenderBox renderBox = context.findRenderObject();
//     final size = renderBox.size;
//     final offset = renderBox.localToGlobal(Offset.zero);
//     return Rect.fromLTWH(offset.dx, offset.dy, size.width, size.height);
//   }

//   void _showPopupRoute(BuildContext context) {
//     Rect renderBoxData = _getRenderBoxData(context);
//     _popupSlideRoute = PopupSlideRoute(
//       asWideAsParent: false,
//       animationDuration: animationDuration,
//       children: [
//         CMActionSelector(
//           children: [
//             CMActionSelectorItem(
//               child: Text(
//                 "Last week",
//                 style: CMTextStyle.actionSelectorItemText,
//               ),
//             ),
//             CMActionSelectorItem(
//               child: Text(
//                 "Last month",
//                 style: CMTextStyle.actionSelectorItemText,
//               ),
//             ),
//             CMActionSelectorItem(
//               child: Text(
//                 "Last 3 month",
//                 style: CMTextStyle.actionSelectorItemText,
//               ),
//             ),
//             CMActionSelectorItem(
//               child: Text(
//                 "Last 6 month",
//                 style: CMTextStyle.actionSelectorItemText,
//               ),
//             ),
//             CMActionSelectorItem(
//               child: Text(
//                 "Year-to-date",
//                 style: CMTextStyle.actionSelectorItemText,
//               ),
//             ),
//           ],
//         ),
//         CMActionSelectorCloseButton()
//       ],
//       parentData: renderBoxData,
//     );
//     Navigator.push(widget.parentContext ?? context, _popupSlideRoute);
//     _popupSlideRoute?.animation?.addStatusListener(_routeAnimationListener);
//   }

//   @override
//   void dispose() {
//     _popupSlideRoute?.animation?.removeStatusListener(_routeAnimationListener);
//     _rotateController?.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     ///TODO check padding
//     return Container(
//       alignment: Alignment.center,
//       constraints: BoxConstraints.expand(height: 95),
//       padding: EdgeInsets.only(left: 20, right: 23),
//       child: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           Padding(
//             padding: EdgeInsets.only(bottom: 2),
//             child: Row(
//               children: <Widget>[
//                 ///TODO check padding value
//                 Padding(
//                   padding: const EdgeInsets.only(right: 8.0),
//                   child: Text(
//                     "REWARDS",
//                     style: CMTextStyle.rewardListTileRewardsTitleTextStyle,
//                   ),
//                 ),
//                 Expanded(
//                   child: Container(
//                     color: CMColors.backgroundPurpleLite,
//                     height: 1,
//                   ),
//                 )
//               ],
//             ),
//           ),
//           Row(
//             children: <Widget>[
//               Material(
//                 color: Colors.transparent,
//                 child: InkWell(
//                   onTap: _handleDateTap,
//                   child: Row(
//                     children: <Widget>[
//                       Icon(
//                         CMIcons.calendar,
//                         size: 14,
//                         color: CMColors.backgroundDeepPurple,
//                       ),
//                       Padding(
//                         padding: EdgeInsets.only(left: 7, right: 20),
//                         child: buildDateTextWidget(widget.rewardDateType),
//                       ),
//                       RotationTransition(
//                         turns: _iconTurns,
//                         child: Icon(
//                           Icons.keyboard_arrow_down,
//                           size: 12,
//                           color: CMColors.purpleAccent,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//               Expanded(
//                 child: Align(
//                   alignment: Alignment.centerRight,
//                   child: RichText(
//                     text: TextSpan(
//                       text: '${widget.amount} ',
//                       style: CMTextStyle.rewardListTileAmountTextStyle,
//                       children: <TextSpan>[
//                         TextSpan(
//                             text: widget.currency,
//                             style: TextStyle(fontWeight: FontWeight.w300)),
//                       ],
//                     ),
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ],
//       ),
//       decoration: BoxDecoration(
//           color: CMColors.rewardListTileBackground,
//           borderRadius: BorderRadius.all(Radius.circular(16))),
//     );
//   }

//   Text buildDateTextWidget(RewardDateTypes rewardDateType) {
//     String text;
//     switch (rewardDateType) {
//       case RewardDateTypes.last_week:
//         {
//           text = 'last week';
//         }
//         break;

//       case RewardDateTypes.last_month:
//         {
//           text = 'last month';
//         }
//         break;
//       case RewardDateTypes.last_3_months:
//         {
//           text = 'last 3 month';
//         }
//         break;
//       case RewardDateTypes.last_6_months:
//         {
//           text = 'last 6 month';
//         }
//         break;
//       case RewardDateTypes.year_to_date:
//         {
//           text = 'year to date';
//         }
//         break;
//     }
//     return Text(
//       text,
//       style: TextStyle(
//           fontSize: 17,
//           height: 1.23,
//           decoration: TextDecoration.underline,
//           color: CMColors.purpleAccent),
//     );
//   }

//   void _routeAnimationListener(AnimationStatus status) {
//     if (status == AnimationStatus.reverse) _handleDateTap();
//   }

//   void _handleDateTap() {
//     if (isPopupShown) {
//       _rotateController.reverse();
//       isPopupShown = false;
//     } else {
//       _rotateController.forward();
//       isPopupShown = true;
//       _showPopupRoute(context);
//     }
//   }
// }
