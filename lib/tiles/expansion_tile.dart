part of '../cm_components.dart';

class CMExpansionTile extends StatefulWidget {
  final List<Widget> tags;
  final String title;
  final String expandedSubtitle;
  final double score;
  final double reward;
  final double free;
  final String stabilityData;
  final bool isChoosed;
  final ValueChanged<bool> onChoose;
  CMExpansionTile(
      {@required this.title,
      this.tags,
      @required this.isChoosed,
      @required this.expandedSubtitle,
      @required this.free,
      @required this.reward,
      @required this.score,
      this.onChoose,
      @required this.stabilityData})
      : assert(title != null),
        assert(expandedSubtitle != null),
        assert(isChoosed != null),
        assert(free != null),
        assert(reward != null),
        assert(score != null),
        assert(stabilityData != null);
  @override
  _CMExpansionTileState createState() => _CMExpansionTileState();
}

class _CMExpansionTileState extends State<CMExpansionTile>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _iconTurns;
  Animation<Color> _colorAnimation;
  final Duration animationDuration = const Duration(milliseconds: 250);
  final EdgeInsets innerPadding =
      EdgeInsets.only(left: 15, top: 19, right: 6, bottom: 23);
  bool isExpanded = false;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(duration: animationDuration, vsync: this);
    _iconTurns = Tween<double>(begin: 0.0, end: 0.5).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeIn,
      ),
    );
    _colorAnimation = ColorTween(
            begin: CMColors.animatedCircleUnselected,
            end: CMColors.backgroundBlack)
        .animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.easeIn,
      ),
    );
  }

  void _handleTap() {
    if (isExpanded) {
      _animationController.reverse();
      if (mounted) isExpanded = false;
    } else {
      if (mounted) isExpanded = true;
      _animationController.forward();
    }
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        CMNodeListTile(
          innerPadding: innerPadding,
          isChoosed: widget.isChoosed,
          onChoose: widget.onChoose,
          duration: animationDuration,
          title: widget.title,
          tags: widget.tags,
          trailing: InkWell(
            onTap: _handleTap,
            child: RotationTransition(
              turns: _iconTurns,
              child: AnimatedBuilder(
                animation: _animationController,
                builder: (context, child) {
                  return Icon(
                    Icons.expand_more,
                    size: 22,
                    color: _colorAnimation.value,
                  );
                },
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              left: innerPadding.left, right: innerPadding.right),
          child: AnimatedBuilder(
            builder: (context, child) {
              return ClipRect(
                  child: Align(
                heightFactor: _animationController.value,
                child: child,
              ));
            },
            animation: _animationController,
            child: Column(
              children: <Widget>[
                CMScoreTile(
                  score: widget.score,
                  stabilityData: widget.stabilityData,
                  reward: widget.reward,
                  free: widget.free,
                ),
                CMUnderlineBox(
                  innerPadding: EdgeInsets.only(top: 23, bottom: 18),
                  child: Column(
                    children: <Widget>[
                      Text(
                        widget.expandedSubtitle,
                        textAlign: TextAlign.justify,
                        style: CMTextStyle.black14pxText,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: CMShareIconsRow(),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
