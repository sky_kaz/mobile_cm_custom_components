part of '../cm_components.dart';

class CMOperationListTile extends StatelessWidget {
  final String address;
  final String amount;
  final String currency;
  final Widget trailing;
  final Widget titleSuffix;
  final IconData icon;
  final VoidCallback onTileTap;
  CMOperationListTile(
      {@required this.address,
      @required this.amount,
      @required this.currency,
      @required this.icon,
      this.titleSuffix,
      this.trailing,
      this.onTileTap})
      : assert(address != null),
        assert(amount != null),
        assert(currency != null),
        assert(icon != null);
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
          padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
        (Set<MaterialState> states) => EdgeInsets.zero,
      )),
      onPressed: onTileTap,
      child: Container(
        height: 69,
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(right: 20),
              height: 45,
              width: 45,
              alignment: Alignment.center,
              child: Icon(
                icon,
                size: 20,
                color: CMColors.backgroundPurpleLite,
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: CMColors.operationListTileIconContainer),
            ),
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                address.length > 27
                                    ? "${address.substring(0, 10)}***${address.substring(address.length - 10)}"
                                    : address,
                                overflow: TextOverflow.ellipsis,
                                style: CMTextStyle.black14pxText,
                              ),
                            ),
                            titleSuffix != null
                                ? titleSuffix
                                : Icon(
                                    Icons.chevron_right,
                                    size: 14,
                                    color: CMColors.backgroundGray,
                                  )
                          ],
                        ),
                        RichText(
                          overflow: TextOverflow.ellipsis,
                          text: TextSpan(
                            text: '$amount ',
                            style: CMTextStyle.amountTextStyle,
                            children: <TextSpan>[
                              TextSpan(
                                  text: currency,
                                  style: CMTextStyle.amountTextStyle.merge(
                                      TextStyle(
                                          fontWeight: FontWeight.w300,
                                          color: CMColors.amountValueText))),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (trailing != null) trailing
                ],
              ),
            ),
          ],
        ),
        padding: EdgeInsets.symmetric(vertical: 12),
      ),
    );
  }
}
