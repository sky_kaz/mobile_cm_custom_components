part of '../cm_components.dart';

class CMCurrencyTile extends StatelessWidget {
  final String amount;
  final String currencyName;
  final String currencyCode;
  final IconData currencyIcon;
  final VoidCallback onTileTap;
  final Color iconContainerColor;
  final Color iconContainerShadowColor;
  CMCurrencyTile({
    @required this.amount,
    @required this.currencyName,
    @required this.currencyCode,
    @required this.currencyIcon,
    @required this.iconContainerShadowColor,
    @required this.iconContainerColor,
    this.onTileTap,
  })  : assert(amount != null),
        assert(currencyName != null),
        assert(currencyIcon != null),
        assert(currencyCode != null),
        assert(iconContainerColor != null),
        assert(iconContainerShadowColor != null);
  @override
  Widget build(BuildContext context) {
    return Ink(
      height: 82,
      decoration: BoxDecoration(
          color: CMColors.backgroundWhite,
          borderRadius: BorderRadius.all(Radius.circular(16)),
          boxShadow: [
            BoxShadow(
                offset: Offset.zero,
                blurRadius: 25,
                color: CMColors.currencyTileShadowColor)
          ]),
      child: InkWell(
        onTap: onTileTap,
        borderRadius: BorderRadius.all(Radius.circular(16)),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 21),
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: iconContainerColor,
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 4),
                          blurRadius: 10,
                          color: iconContainerShadowColor)
                    ]),
                child: Icon(
                  currencyIcon,
                  size: 24,
                  color: CMColors.backgroundWhite,
                ),
              ),
              Text(
                currencyName,
                style: CMTextStyle.currencyTileTitle.copyWith(
                  fontFamily: 'cm_components/fonts/Montserrat-Black.ttf'
                ),
              ),
              //TODO размер отступа линии на всех экранах разный
              Expanded(
                  child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Container(
                  color: CMColors.underlineBorder,
                  height: 1,
                ),
              )),
              //TODO проверить стили текста
              RichText(
                text: TextSpan(
                  text: '$amount ',
                  style: CMTextStyle.currencyTileSubtitle.copyWith(
                      fontFamily: 'cm_components/fonts/Montserrat-Black.ttf'
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: currencyCode,
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            color: CMColors.backgroundBlack,
                            fontFamily: 'cm_components/fonts/Montserrat-Black.ttf')),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  // String _formatAmount(double value){
  //   var tmp = '$amount '.replaceFirst('.', ',');
  //
  //   if (tmp.indexOf(',') > 0) {
  //     final tmpSplit = tmp.split(',');
  //     if (tmpSplit.length > 1 && tmpSplit[1].length > 6) {
  //       tmp = tmpSplit[1].substring(0, 6);
  //       tmp = '${tmpSplit[0]},$tmp ';
  //     }
  //   }
  //
  //   return tmp;
  // }
}
