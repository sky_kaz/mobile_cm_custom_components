part of '../cm_components.dart';

class CMSwitchTile extends StatelessWidget {
  final String title;
  final TextStyle titleStyle;
  final ValueChanged<bool> onChanged;
  final bool isActive;
  final EdgeInsets padding;
  CMSwitchTile(
      {@required this.title,
      @required this.onChanged,
      @required this.isActive,
      this.titleStyle = CMTextStyle.switchTileText,
      this.padding = const EdgeInsets.symmetric(vertical: 15)})
      : assert(title != null),
        assert(onChanged != null),
        assert(isActive != null);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Row(
        children: [
          Expanded(
            child: Text(
              title,
              style: titleStyle,
            ),
          ),
          CMSwitch(
            value: isActive,
            onChanged: (value) => onChanged(value),
            activeColor: CMColors.switchTileActiveSwitch,
            trackColor: CMColors.switchTileDisabledSwitch,
          ),
        ],
      ),
    );
  }
}
