part of '../cm_components.dart';

class CMWalletListTile extends StatefulWidget {
  final IconData icon;
  final VoidCallback onTileTap;
  final String title;
  final String subTitle;
  CMWalletListTile(
      {@required this.icon,
      @required this.title,
      @required this.subTitle,
      @required this.onTileTap})
      : assert(icon != null),
        assert(title != null),
        assert(subTitle != null),
        assert(onTileTap != null);
  @override
  _CMWalletListTileState createState() => _CMWalletListTileState();
}

class _CMWalletListTileState extends State<CMWalletListTile> {
  static const Duration animationDuration = Duration(milliseconds: 250);
  bool isTapped = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      onTap: () {
        setState(() {
          isTapped = !isTapped;
        });
        Future.delayed(animationDuration, () {
          widget.onTileTap();
          setState(() {
            isTapped = !isTapped;
          });
        });
        ;
      },
      child: AnimatedContainer(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: isTapped
                ? CMColors.walletListTileBackgroundSelected
                : CMColors.walletListTileBackgroundUnselected),
        duration: animationDuration,
        padding: EdgeInsets.only(top: 20, bottom: 20, left: 20, right: 6),
        child: Row(
          children: [
            AnimatedContainer(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 4),
                        blurRadius: 10,
                        color: CMColors.walletListTileShadow)
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: !isTapped
                      ? CMColors.walletListTileBackgroundSelected
                      : CMColors.walletListTileBackgroundUnselected),
              margin: EdgeInsets.only(right: 17),
              alignment: Alignment.center,
              width: 70,
              height: 70,
              duration: animationDuration,
              child: AnimatedColorIcon(
                  color: isTapped
                      ? CMColors.walletListTileBackgroundSelected
                      : CMColors.walletListTileBackgroundUnselected,
                  size: 29,
                  icon: widget.icon,
                  duration: animationDuration),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: AnimatedDefaultTextStyle(
                        style: isTapped
                            ? CMTextStyle.walletListTileTitleSelected
                            : CMTextStyle.walletListTileTitleUnselected,
                        duration: animationDuration,
                        child: Text(widget.title)),
                  ),
                  AnimatedDefaultTextStyle(
                      style: isTapped
                          ? CMTextStyle.walletListTileSubtitleSelected
                          : CMTextStyle.walletListTileSubtitleUnselected,
                      duration: animationDuration,
                      child: Text(widget.subTitle))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
