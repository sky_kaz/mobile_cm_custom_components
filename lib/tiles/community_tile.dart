import 'package:cm_components/cm_components.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CommunityTile extends StatelessWidget {
  final String tileName;
  final String iconAsset;
  final Function onPressed;

  const CommunityTile({this.tileName, this.iconAsset, this.onPressed,});

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: onPressed,
      padding: EdgeInsets.zero,
      child: Container(
        height: 64,
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: CMColors.underlineBorder)),
        ),
        child: Row(
          children: [
            SizedBox(
              width: 40,
              child: Center(
                child: SvgPicture.asset(
                  iconAsset,
                ),
              ),
            ),
            SizedBox(width: 16),
            Text(
              tileName,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w400,
                fontSize: 17,
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}

class SettingsLogoutTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      onPressed: (){},
      padding: EdgeInsets.zero,
      child: Container(
        height: 64,
        child: Row(
          children: [
            SizedBox(
              width: 40,
              child: Center(
                child: SvgPicture.asset(
                  "cm_components/assets/logout.svg",
                ),
              ),
            ),
            SizedBox(width: 16),
            Text(
              "Logout",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromRGBO(225, 66, 66, 1),
                fontWeight: FontWeight.w700,
                fontSize: 17,
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
