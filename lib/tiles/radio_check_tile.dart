part of '../cm_components.dart';

class CMRadioCheckTile extends StatelessWidget {
  final ValueChanged<String> onChanged;
  final String value;
  final String searchTerm;
  final String groupValue;
  final String currency;
  final IconData currencyIcon;
  final Color iconContainerColor;
  final Color iconContainerShadowColor;
  final bool radioEnabled;
  CMRadioCheckTile(
      {@required this.onChanged,
      @required this.currency,
      @required this.currencyIcon,
      @required this.iconContainerColor,
      @required this.value,
      @required this.searchTerm,
      @required this.groupValue,
      @required this.iconContainerShadowColor,
      @required this.radioEnabled})
      : assert(onChanged != null),
        assert(currency != null),
        assert(currencyIcon != null),
        assert(iconContainerColor != null),
        assert(iconContainerShadowColor != null),
        assert(value != null),
        assert(searchTerm != null),
        assert(groupValue != null),
        assert(radioEnabled != null);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 21),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 20),
            width: 50,
            height: 50,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: iconContainerColor,
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 4),
                      blurRadius: 10,
                      color: iconContainerShadowColor)
                ]),
            child: Icon(
              currencyIcon,
              size: 24,
              color: CMColors.backgroundWhite,
            ),
          ),
          Expanded(
            child: SubstringHighlight(
              text: currency,
              textStyle: CMTextStyle.checkboxTileCurrency,
              term: searchTerm,
              textStyleHighlight:
                  CMTextStyle.checkboxTileCurrency.copyWith(color: Colors.red),
            ),
          ),
          radioEnabled
              ? Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Color(0xFFC5C4EE),
                  ),
                  child: Radio<String>(
                    value: value,
                    groupValue: groupValue,
                    onChanged: onChanged,
                    activeColor: Color(0xFF8E54E9),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}
