part of '../cm_components.dart';

class CMScoreTile extends StatelessWidget {
  final double score;
  final double reward;
  final double free;
  final String stabilityData;
  CMScoreTile(
      {@required this.score,
      @required this.reward,
      @required this.free,
      @required this.stabilityData})
      : assert(score != null),
        assert(reward != null),
        assert(free != null),
        assert(stabilityData != null);
  @override
  Widget build(BuildContext context) {
    return CMUnderlineBox(
      innerPadding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTitle("Score"),
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: _buildSubtitle("$score"),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTitle("Reward"),
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: _buildSubtitle("${reward.toStringAsFixed(2)}%"),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTitle("Free"),
              Padding(
                  padding: const EdgeInsets.only(top: 2),
                  child: _buildSubtitle("${free.toStringAsFixed(2)}%"))
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              //TODO встроить отрисовку графика
              _buildTitle("Stability"),
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: _buildSubtitle("$score"),
              )
            ],
          ),
        ],
      ),
    );
  }

  Text _buildTitle(String title) {
    return Text(
      title,
      style: TextStyle(fontSize: 12, color: CMColors.scoreTileText),
    );
  }

  Text _buildSubtitle(String subTitle) {
    return Text(
      subTitle,
      style: CMTextStyle.scoreTitleSubtitleTextStyle,
    );
  }
}
