part of '../cm_components.dart';

class CMCheckboxTile extends StatelessWidget {
  final ValueChanged<bool> onChanged;
  final bool isChecked;
  final String currency;
  final IconData currencyIcon;
  final Color iconContainerColor;
  final Color iconContainerShadowColor;
  CMCheckboxTile(
      {@required this.onChanged,
      @required this.currency,
      @required this.currencyIcon,
      @required this.iconContainerColor,
      @required this.isChecked,
      @required this.iconContainerShadowColor})
      : assert(onChanged != null),
        assert(currency != null),
        assert(currencyIcon != null),
        assert(iconContainerColor != null),
        assert(iconContainerShadowColor != null),
        assert(isChecked != null);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          margin: EdgeInsets.only(right: 20),
          width: 50,
          height: 50,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: iconContainerColor,
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 4),
                    blurRadius: 10,
                    color: iconContainerShadowColor)
              ]),
          child: Icon(
            currencyIcon,
            size: 24,
            color: CMColors.backgroundWhite,
          ),
        ),
        Expanded(
          child: Text(
            currency,
            style: CMTextStyle.checkboxTileCurrency,
          ),
        ),
        CMCheckbox(
          onChanged: onChanged,
          isChecked: isChecked,
        )
      ],
    );
  }
}
