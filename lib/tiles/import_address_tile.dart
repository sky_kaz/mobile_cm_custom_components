part of '../cm_components.dart';

class CMImportAddressTile extends StatefulWidget {
  final String svgAsset;
  final VoidCallback onTileTap;
  final String title;
  final String subTitle;
  CMImportAddressTile(
      {@required this.svgAsset,
      @required this.title,
      @required this.subTitle,
      @required this.onTileTap})
      : assert(svgAsset != null),
        assert(title != null),
        assert(subTitle != null),
        assert(onTileTap != null);
  @override
  _CMImportAddressTileState createState() => _CMImportAddressTileState();
}

class _CMImportAddressTileState extends State<CMImportAddressTile> {
  static const Duration animationDuration = Duration(milliseconds: 250);
  bool isTapped = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.all(Radius.circular(10)),
      onTap: () {
        setState(() {
          isTapped = !isTapped;
        });
        Future.delayed(animationDuration, () {
          widget.onTileTap();
          setState(() {
            isTapped = !isTapped;
          });
        });
      },
      child: AnimatedContainer(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: isTapped
                ? CMColors.importAddressTileBackgroundSelected
                : CMColors.importAddressTileBackgroundUnselected),
        duration: animationDuration,
        padding: EdgeInsets.symmetric(vertical: 30, horizontal: 8),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: RepaintBoundary(
                      child: SvgPicture.asset(
                        widget.svgAsset,
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      AnimatedDefaultTextStyle(
                        textAlign: TextAlign.center,
                        style: isTapped
                            ? CMTextStyle.importAddressTileSubtitleSelected
                            : CMTextStyle.importAddressTileSubtitleUnselected,
                        duration: animationDuration,
                        child:
                            Text(widget.subTitle, textAlign: TextAlign.center),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: AnimatedDefaultTextStyle(
                            style: isTapped
                                ? CMTextStyle.importAddressTileTitleSelected
                                : CMTextStyle.importAddressTileTitleUnselected,
                            duration: animationDuration,
                            child: Text(
                              widget.title,
                              textAlign: TextAlign.center,
                            )),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
