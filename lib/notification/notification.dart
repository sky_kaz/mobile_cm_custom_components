part of '../cm_components.dart';

class CMNotification extends StatelessWidget {
  final Widget child;
  final Color color;
  CMNotification({@required this.child, this.color = CMColors.backgroundWhite})
      : assert(child != null);
  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 26, horizontal: 16),
        padding: EdgeInsets.only(top: 21, left: 16, right: 16, bottom: 21),
        width: 382,
        child: child,
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.all(Radius.circular(16)),
            boxShadow: [
              BoxShadow(
                  offset: Offset.zero,
                  blurRadius: 20,
                  color: CMColors.backgroundBlack.withOpacity(0.1))
            ]),
      ),
    );
  }
}

//TODO после показа удалить
class CMNotificationTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CMFlatButton(
      text: "show notification",
      onPressed: () {
        Navigator.push(
            context,
            PopupNotificationRoute(
              animationDuration: Duration(milliseconds: 300),
              notification: CMNotification(
                child: RichText(
                  overflow: TextOverflow.ellipsis,
                  text: TextSpan(
                    text: 'There are still ',
                    style: CMTextStyle.heading2,
                    children: <TextSpan>[
                      //TODO проверить размер и цвет шрифта
                      TextSpan(
                          text: '3',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: CMColors.backgroundBlack)),
                      TextSpan(
                        text: ' attempts',
                      ),
                    ],
                  ),
                ),
              ),
            ));
      },
    );
  }
}

class SettingsNotification extends StatelessWidget {
  final Widget child;
  final Color color;
  SettingsNotification(
      {@required this.child, this.color = CMColors.backgroundWhite})
      : assert(child != null);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
      alignment: Alignment.center,
      height: 100,
      //width: 382,
      child: child,
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
                offset: Offset.zero,
                blurRadius: 20,
                color: CMColors.backgroundBlack.withOpacity(0.1))
          ]),
    );
  }
}
