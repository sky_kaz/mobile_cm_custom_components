part of '../cm_components.dart';

class CMLogoWithCurves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _w = MediaQuery.of(context).size.width;
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 40),
        child: SizedBox(
          width: _w * 0.7,
          child: Image.asset(
            "cm_components/assets/signin-logo.png",
          ),
        ),
      ),
    );
  }
}
