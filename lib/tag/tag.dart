part of '../cm_components.dart';

class CMTag extends StatelessWidget {
  final Color iconAndTextColor;
  final Color iconContainerColor;
  final Color backgroundColor;
  final String tagText;

  CMTag(
      {@required this.iconAndTextColor,
      @required this.backgroundColor,
      @required this.iconContainerColor,
      @required this.tagText})
      : assert(iconAndTextColor != null),
        assert(backgroundColor != null),
        assert(iconContainerColor != null),
        assert(tagText != null);
  const CMTag.promoted()
      : iconAndTextColor = CMColors.promotedTagIcon,
        backgroundColor = CMColors.promotedTagBackground,
        iconContainerColor = CMColors.promotedTagIconContainer,
        tagText = 'promoted';
  const CMTag.best()
      : iconAndTextColor = CMColors.bestTagIcon,
        backgroundColor = CMColors.bestTagBackground,
        iconContainerColor = CMColors.bestTagIconContainer,
        tagText = 'best';
  const CMTag.popular()
      : iconAndTextColor = CMColors.popularTagIcon,
        backgroundColor = CMColors.popularTagBackground,
        iconContainerColor = CMColors.popularTagIconContainer,
        tagText = 'popular';
  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: DecoratedBox(
        decoration: BoxDecoration(
            color: backgroundColor, borderRadius: BorderRadius.circular(3)),
        child: Row(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: iconContainerColor,
                  borderRadius: BorderRadius.circular(3)),
              height: 19,
              width: 19,
              child: Icon(
                CMIcons.tag,
                size: 12,
                color: iconAndTextColor,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 6),
              child: Text(
                tagText,
                style: TextStyle(fontSize: 12, color: iconAndTextColor),
              ),
            )
          ],
        ),
      ),
    );
  }
}
