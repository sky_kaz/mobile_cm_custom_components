part of "../cm_components.dart";

class CMShareIconsRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //TODO действие при нажатии
    return SizedBox(
      width: 165,
      height: 24,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            constraints: BoxConstraints.tightFor(width: 24),
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: FaIcon(
              FontAwesomeIcons.twitter,
              size: 24,
              color: CMColors.shareIcons,
            ),
          ),
          IconButton(
            constraints: BoxConstraints.tightFor(width: 24),
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: FaIcon(
              FontAwesomeIcons.mediumM,
              size: 24,
              color: CMColors.shareIcons,
            ),
          ),
          IconButton(
            constraints: BoxConstraints.tightFor(width: 24),
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: FaIcon(
              FontAwesomeIcons.telegramPlane,
              size: 24,
              color: CMColors.shareIcons,
            ),
          ),
          //TODo Globe icon не та
          IconButton(
            constraints: BoxConstraints.tightFor(width: 24),
            padding: EdgeInsets.zero,
            onPressed: () {},
            icon: FaIcon(
              FontAwesomeIcons.globe,
              size: 24,
              color: CMColors.shareIcons,
            ),
          ),
        ],
      ),
    );
  }
}
