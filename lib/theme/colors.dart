part of '../cm_components.dart';

class CMColors {
  // PRIMARY
  static const primaryPurple = Color(0xff706FD3);
  static const primaryPurpleLite = Color(0xff7F5EFF);

  ///TODO check primaryBlue
  static const primaryBlue = Color(0xff4876e6);

  // ACCENT
  static const purpleAccent = Color(0xFF7061B6);
  // CALLBACK
  static const error = Color(0xFFE14242);
  // BACKGROUND
  static const backgroundWhite = Color(0xFFFFFFFF);
  static const backgroundBlack = Color(0xFF000000);
  static const backgroundBlackLite = Color(0xFF3F3F3F);
  static const backgroundGrayLite = Color(0xFFF3F3F3);

  static const backgroundGray = Color(0xFFBEBEBE);

  static const backgroundPurpleLite = Color(0xFFAAA4FC);
  static const backgroundDeepPurple = Color(0xFF3F3689);

  //TEXT FIELD
  static const textFieldBorderBackground = Color(0xFFCFCFF7);

  //TITLE
  static const title = Color(0xFF706FD3);
  //HEADING2
  static const heading2 = Color(0xFF888888);
  //NORMAL
  static const normal = Color(0xFF37474F);

  //NAVIGATION BAR
  static const navigationBarBackground = Color(0xff695FE1);
  static const navigationBarShadow = Color(0x80695FE1);
  static const navigationBarIconUnselected = Color(0xFFC5C4EE);

  //REWARD LIST TILE
  static const rewardListTileBackground = Color(0xFFECEBFF);

  //OPERATION LIST TILE
  static const operationListTileIconContainer = Color(0xFFF6F4FF);

  //UNDERLINE BORDER
  static const underlineBorder = Color(0xFFE0E3F3);

  //TAGS
  //PROMOTED
  static const promotedTagBackground = Color(0xFFD8EFFF);
  static const promotedTagIcon = Color(0xFF008DE4);
  static const promotedTagIconContainer = Color(0xFFB2DFFF);

  //BEST
  static const bestTagBackground = Color(0xFFFFE8E0);
  static const bestTagIcon = Color(0xFFFD764A);
  static const bestTagIconContainer = Color(0xFFFECEBE);

  //POPULAR
  static const popularTagBackground = Color(0xFFEEECFF);
  static const popularTagIcon = purpleAccent;
  static const popularTagIconContainer = Color(0xFFD2CCFF);

  //NODE LIST ANIMATED CIRCLE
  static const animatedCircleUnselected = Color(0xFFC5C4EE);
  //SCORE TILE
  static const scoreTileText = Color(0xFFABABAB);
  //SHARE ICONS
  static const shareIcons = Color(0xFF98B9EA);
  //DASHED LINE PAINTER
  static const dashedUnderline = Color(0xFFDADADA);
  //EXPANDED TEXT
  static const expandedTextLeft = Color(0xFFABABAB);
  static const expandedTextRight = Color(0xFF393939);
  //SEPARATOR
  static const separatorBackground = Color(0xFF8E54E9);
  //OPERATION ICONS
  //RECEIVE
  static const receiveOperationIconBackground = Color(0xFF0FB774);
  static const receiveOperationIconShadow = Color(0xFF87dbba);
  //SEND
  static const sendOperationIconBackground = Color(0xFFE14242);
  static const sendOperationIconShadow = Color(0xFFf0a1a1);
  //REWARD
  static const rewardOperationIconBackground = Color(0xFF008DE4);
  static const rewardOperationIconShadow = Color(0xFF80c6f2);
  //STACKING
  static const stackingOperationIconBackground = Color(0xFF8E54E9);
  static const stackingOperationIconShadow = Color(0xFFc7aaf4);
  //UNSTACKING
  static const unstackingOperationIconBackground = Color(0xFFFD764A);
  static const unstackingOperationIconShadow = Color(0xFFfebba5);
  //VOTING
  static const votingOperationIconBackground = Color(0xFFFFA412);
  static const votingOperationIconShadow = Color(0xFFfebba5);
  //FEES
  static const feesOperationIconBackground = Color(0xFF6625DE);
  static const feesOperationIconShadow = Color.fromRGBO(102, 37, 222, 0.5);
  //TAB BAR
  static const tabBarBackground = Color(0xFFECEBFF);
  static const tabBarIndicator = Color(0xFF706FD3);
  //FLAT BUTTON
  static const flatButtonBackground = Color(0xFFFE6C3B);
  static const flatButtonShadow = Color(0xFFffb69d);
  static const flatButtonDisabled = Color(0xFFF6F4FF);
  //NOTIFICATION
  static const notificationShadow = Color(0xFF808080);
  //WALLET LIST TILE
  static const walletListTileBackgroundSelected = Color(0xFF706FD3);
  static const walletListTileBackgroundUnselected = Color(0xFFF6F4FF);
  static const walletListTileShadow = Color(0xFFb4aff0);
  //ADD AN ADDRESS TILE
  static const addAnAddressTileBackgroundSelectedGradientLeft =
      Color.fromRGBO(71, 118, 230, 1);
  static const addAnAddressTileBackgroundSelectedGradientRight =
      Color.fromRGBO(142, 84, 233, 1);
  static const addAnAddressTileBorderColor = Color.fromRGBO(197, 196, 238, 1);
  static const addAnAddressTileBackgroundUnselected = Color(0xFFF6F4FF);
  static const addAnAddressTileShadow = Color(0xFFb4aff0);
  //IMPORT ADDRESS TILE
  static const importAddressTileTitleUnselected =
      Color.fromRGBO(142, 84, 233, 1);
  static const importAddressTileBackgroundSelected = Color(0xFF706FD3);
  static const importAddressTileBackgroundUnselected = Color(0xFFF6F4FF);
  static const importAddressTileShadow = Color(0xFFb4aff0);
  //CHECKBOX
  static const checkboxUnselected = Color(0xFFC5C4EE);
  static const checkboxSelected = Color(0xFF8E54E9);
  //SEED CONTAINER
  static const seedContainerBackground = Color(0xFFECEBFF);
  static const seedContainerText = Color(0xFF706FD3);
  //DASHED BORDER
  static const dashedBorder = Color(0xFFCFD8DC);
  //SELECTABLE SEED CONTAINER
  static const selectableSeedContainerText = Color(0xFF90A4AE);

  static const pinCodeUnderLine = Color(0xFFC4C4C4);
  static const digitButtonDelete = Color(0xFFD6D6D6);
  static const faceIdIcon = Color(0xFF8E54E9);
  //SWITCH TILE
  static const switchTileActiveSwitch = Color(0xFF8E54E9);
  static const switchTileDisabledSwitch = Color(0xFFDADADA);
  static const switchTileSwitchShadow = Color(0xFF808080);
  //ACTION SELECTOR
  static const actionSelectorShadow = Color(0xFFe6e6e6);
  static const actionSelectorCancelButton = Color(0xFFECEBFF);
  static const actionSelectorText = Color(0xFF706FD3);
  //FLAT BUTTON DISABLED
  static const flatButtonDisabledColor = Color(0xFFECEBFF);
  //APPBAR LEADING
  static const appBarLeading = Color(0xFFC5C4EE);
  //SHARE BUTTON
  static const shareButtonBackground = Color(0xFF008DE4);
  static const shareButtonShadow = Color(0xFF80c6f2);
  //CURRENCY TILE
  static const currencyTileShadowColor = Color(0xFFd9d9d9);
  //RAISED BUTTON
  static const raisedButtonBackground = Color(0xFFFE6C3B);
  static const raisedButtonShadowOrange = Color(0xFFffb69d);
  static const raisedButtonShadowBlue = Color(0xff716fd3);
  static const raisedButtonDisabled = Color(0xFFF6F4FF);

  //TRANSACTIONS SCREEN
  static const transactionsFeeValueText = Color(0xFF008DE4);

  //TEZOS BACKGROUND GRADIENTS
  static const tezosBackgroundGradientStart = Color(0xFF4776E6);
  static const tezosBackgroundGradientEnd = Color(0xFF8E54E9);

  //BALANCE WIDGET
  static const balanceText = Color(0xFF98B9EA);
  static const dollarRate = Color(0xFFCFCFF7);

  // WALLET WIDGET
  static const amountValueText = Color(0xFF393939);
  static const amountNameText = Color(0xFFABABAB);

  //COIN ICON
  static const coinIconEtherium = Color(0xFF77A4E5);
  static const coinIconOrbs = Color(0xFFFFA412);
  static const coinIconIcon = Color(0xFF695FE1);
  static const coinIconIost = Color(0xFFFD764A);

  // COIN ICON SHADOW
  static const coinIconShadowTezos = Color(0xFF6a5fe1);
  static const coinIconShadowEtherium = Color(0xFF98B9EA);
  static const coinIconShadowOrbs = Color(0xFFED975D);
  static const coinIconShadowIcon = Color(0xFF6a5fe1);
  static const coinIconShadowIost = Color(0xFFfd774a);

  // OPERATION FEE
  static const operationFeeTo = Color(0xFFE14242);
  static const operationFeeFrom = Color(0xFF0FB774);
  static const operationCurrencySelected = Color(0xFF7F5EFF);
  static const operationCurrencyUnSelected = Color(0xFFABABAB);
  static const transactionEditIcon = Color(0xFFFD764A);

  static const rewardScreenBackground = Color(0xFFE5E5E5);

  static const socialFacebook = Color(0xFF706FD3);
  static const socialApple = Color(0xFF000000);
  static const socialGoogle = Color(0xFFFFA412);
  static const socialLinkedIn = Color(0xFF695FE1);

  static const mutedText = Color(0xFF979797);

  // RAM/GAS
  static const buyGamRam = Color(0xFF008DE4);
  static const buyGamRamNumeric = Color(0xFFAAA4FC);
  static const buyGamRamOperationApprovalText = Color(0xFF888888);
  static const buyGamRamOperationValidateCurrency = Color(0xFF3F3689);

  //TEZOS BACKGROUND GRADIENTS
  static const addWalletInfoBackgroundGradientStart = Color(0xFFFE6C3B);
  static const addWalletInfoBackgroundGradientEnd = Color(0xFFFEF0E9);

  //EMAIL (SETTINGS) BACKGROUND GRADIENTS
  static const emailSettingsBackgroundGradientStart = Color(0xFF4776E6);
  static const emailSettingsBackgroundGradientEnd = Color(0xFF8E54E9);
  static const yourEmailText = Color(0xFFC5C4EE);

  // erc/snip token-colors
  static const unSelectedTokenIcon = Color(0xFF008DE4);
  static const selectedTokenIcon = Color(0xFFFFFFFF);
  static const encodedTokenIcon = Color(0xFFFE6C3B);

  static const unSelectedTokenBackground = Color(0xFFD8EFFF);
  static const selectedTokenBackground = Color(0xFF4776E6);
  static const encodedTokenBackground = Color(0xFFFFE8E0);

  static const unSelectedTokenName = Color(0xFF000000);
  static const selectedTokenName = Color(0xFFFFFFFF);
  static const encodedTokenName = Color(0xFF000000);

  static const unSelectedTokenBalance = Color(0xFF008DE4);
  static const selectedTokenBalance = Color(0xFFFFFFFF);

  static const unSelectedTokenCurrency = Color(0xFF77A4E5);
  static const selectedTokenCurrency = Color(0xFFB2DFFF);
  static const mnemonicPhraseButtonColor = Color.fromRGBO(112, 111, 211, 0.5);

  /// wallet types represented by colors
  static const oneSeedColor = Color(0xFFFF5722);
  static const publicColor = Color(0xFF8180d6);

  static const dividerColorOnQRCodeScannPage = Color.fromRGBO(224, 227, 243, 1);

  const CMColors();
}
