part of '../cm_components.dart';

class UISpacing {
  static const double xxs = 4;
  static const double xs = 8;
  static const double s = 12;
  static const double m = 16;
  static const double l = 20;
  static const double xl = 24;
  static const double xxl = 28;
  static const double xxxl = 32;

  static Widget getXXS() =>
      Padding(padding: EdgeInsets.symmetric(vertical: xxs));

  static Widget getXS() => Padding(padding: EdgeInsets.symmetric(vertical: xs));

  static Widget getS() => Padding(padding: EdgeInsets.symmetric(vertical: s));

  static Widget getM() => Padding(padding: EdgeInsets.symmetric(vertical: m));

  static Widget getL() => Padding(padding: EdgeInsets.symmetric(vertical: l));

  static Widget getXL() => Padding(padding: EdgeInsets.symmetric(vertical: xl));

  static Widget getXXL() =>
      Padding(padding: EdgeInsets.symmetric(vertical: xxl));

  static Widget getXXXL() =>
      Padding(padding: EdgeInsets.symmetric(vertical: xxxl));
}
