part of '../cm_components.dart';

class AppTheme {
  AppTheme._();

  static FontWeight _getFontWeight(int weight) {
    switch (weight) {
      case 100:
        return FontWeight.w100;
      case 200:
        return FontWeight.w200;
      case 300:
        return FontWeight.w300;
      case 400:
        return FontWeight.w300;
      case 500:
        return FontWeight.w400;
      case 600:
        return FontWeight.w500;
      case 700:
        return FontWeight.w600;
      case 800:
        return FontWeight.w700;
      case 900:
        return FontWeight.w900;
    }
    return FontWeight.w400;
  }

  static TextStyle getTextStyle(
    TextStyle textStyle, {
    String fontFamily = 'SofiaPro',
    int fontWeight = 500,
    bool muted = false,
    double letterSpacing = 0.15,
    Color color,
    TextDecoration decoration = TextDecoration.none,
    double height,
    double wordSpacing = 0,
    double fontSize,
  }) {
    double finalFontSize = fontSize != null ? fontSize : textStyle.fontSize;

    if (color == null) {
      color = muted ? textStyle.color.withAlpha(200) : textStyle.color;
    } else {
      color = muted ? color.withAlpha(200) : color;
    }

    return TextStyle(
        fontFamily: fontFamily,
        fontSize: finalFontSize,
        fontWeight: _getFontWeight(fontWeight),
        letterSpacing: letterSpacing,
        color: color,
        decoration: decoration,
        height: height,
        wordSpacing: wordSpacing);
  }

  //App Bar Text
  static final TextTheme lightAppBarTextTheme = TextTheme(
    headline1: TextStyle(fontSize: 102, color: CMColors.backgroundBlack),
    headline2: TextStyle(fontSize: 64, color: CMColors.backgroundBlack),
    headline3: TextStyle(fontSize: 51, color: CMColors.backgroundBlack),
    headline4: TextStyle(fontSize: 36, color: CMColors.backgroundBlack),
    headline5: TextStyle(fontSize: 25, color: CMColors.backgroundBlack),
    headline6: TextStyle(fontSize: 18, color: CMColors.backgroundBlack),
    subtitle1: TextStyle(fontSize: 17, color: CMColors.backgroundBlack),
    subtitle2: TextStyle(fontSize: 15, color: CMColors.backgroundBlack),
    bodyText1: TextStyle(fontSize: 16, color: CMColors.backgroundBlack),
    bodyText2: TextStyle(fontSize: 14, color: CMColors.backgroundBlack),
    button: TextStyle(fontSize: 15, color: CMColors.backgroundBlack),
    caption: TextStyle(fontSize: 13, color: CMColors.backgroundBlack),
    overline: TextStyle(fontSize: 11, color: CMColors.backgroundBlack),
  );
  static final TextTheme darkAppBarTextTheme = TextTheme(
    headline1: TextStyle(fontSize: 102, color: CMColors.backgroundWhite),
    headline2: TextStyle(fontSize: 64, color: CMColors.backgroundWhite),
    headline3: TextStyle(fontSize: 51, color: CMColors.backgroundWhite),
    headline4: TextStyle(fontSize: 36, color: CMColors.backgroundWhite),
    headline5: TextStyle(fontSize: 25, color: CMColors.backgroundWhite),
    headline6: TextStyle(fontSize: 18, color: CMColors.backgroundWhite),
    subtitle1: TextStyle(fontSize: 17, color: CMColors.backgroundWhite),
    subtitle2: TextStyle(fontSize: 15, color: CMColors.backgroundWhite),
    bodyText1: TextStyle(fontSize: 16, color: CMColors.backgroundWhite),
    bodyText2: TextStyle(fontSize: 14, color: CMColors.backgroundWhite),
    button: TextStyle(fontSize: 15, color: CMColors.backgroundWhite),
    caption: TextStyle(fontSize: 13, color: CMColors.backgroundWhite),
    overline: TextStyle(fontSize: 11, color: CMColors.backgroundWhite),
  );

  //Text Themes
  static final TextTheme lightTextTheme = TextTheme(
    headline1: TextStyle(fontSize: 102, color: CMColors.backgroundBlack),
    headline2: TextStyle(fontSize: 64, color: CMColors.backgroundBlack),
    headline3: TextStyle(fontSize: 51, color: CMColors.backgroundBlack),
    headline4: TextStyle(fontSize: 36, color: CMColors.backgroundBlack),
    headline5: TextStyle(fontSize: 25, color: CMColors.backgroundBlack),
    headline6: TextStyle(fontSize: 18, color: CMColors.backgroundBlack),
    subtitle1: TextStyle(fontSize: 17, color: CMColors.backgroundBlack),
    subtitle2: TextStyle(fontSize: 15, color: CMColors.backgroundBlack),
    bodyText1: TextStyle(fontSize: 16, color: CMColors.backgroundBlack),
    bodyText2: TextStyle(fontSize: 14, color: CMColors.backgroundBlack),
    button: TextStyle(fontSize: 15, color: CMColors.backgroundBlack),
    caption: TextStyle(fontSize: 13, color: CMColors.backgroundBlack),
    overline: TextStyle(fontSize: 11, color: CMColors.backgroundBlack),
  );

  //Color Themes
  static final ThemeData lightTheme = ThemeData(
    fontFamily: 'Montserrat',
    brightness: Brightness.light,
    primaryColor: CMColors.primaryPurple,
    canvasColor: Colors.transparent,
    backgroundColor: CMColors.backgroundWhite,
    scaffoldBackgroundColor: CMColors.backgroundWhite,
    appBarTheme: AppBarTheme(
      textTheme: lightAppBarTextTheme,
      actionsIconTheme: IconThemeData(
        color: CMColors.backgroundBlack,
      ),
      color: CMColors.backgroundWhite,
      iconTheme: IconThemeData(color: CMColors.backgroundBlack, size: 24),
    ),
    navigationRailTheme: NavigationRailThemeData(
        selectedIconTheme:
            IconThemeData(color: CMColors.primaryPurple, opacity: 1, size: 24),
        unselectedIconTheme: IconThemeData(
            color: CMColors.backgroundBlack, opacity: 1, size: 24),
        backgroundColor: CMColors.backgroundWhite,
        elevation: 3,
        selectedLabelTextStyle: TextStyle(color: CMColors.primaryPurple),
        unselectedLabelTextStyle: TextStyle(color: CMColors.backgroundBlack)),
    colorScheme: ColorScheme.light(
        primary: CMColors.primaryPurple,
        onPrimary: Colors.white,
        primaryVariant: Color(0xff0055ff),
        secondary: CMColors.backgroundBlack,
        secondaryVariant: Color(0xff3cd278),
        onSecondary: Colors.white,
        surface: Color(0xffe2e7f1),
        background: Color(0xfff3f4f7),
        onBackground: CMColors.backgroundBlack),
    cardTheme: CardTheme(
      color: Colors.white,
      shadowColor: Colors.black.withOpacity(0.4),
      elevation: 1,
      margin: EdgeInsets.all(0),
    ),
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: TextStyle(fontSize: 15, color: Color(0xaa495057)),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        borderSide: BorderSide(width: 1, color: CMColors.primaryPurple),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        borderSide: BorderSide(width: 1, color: Colors.black54),
      ),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          borderSide: BorderSide(width: 1, color: Colors.black54)),
    ),
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    textTheme: lightTextTheme,
    indicatorColor: Colors.white,
    disabledColor: Color(0xffdcc7ff),
    floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: CMColors.primaryPurple,
        splashColor: Colors.white.withAlpha(100),
        highlightElevation: 8,
        elevation: 4,
        focusColor: CMColors.primaryPurple,
        hoverColor: CMColors.primaryPurple,
        foregroundColor: Colors.white),
    dividerColor: Color(0xffd1d1d1),
    errorColor: Color(0xfff0323c),
    cardColor: Colors.white,
    accentColor: CMColors.primaryPurple,
    popupMenuTheme: PopupMenuThemeData(
      color: CMColors.backgroundWhite,
      textStyle: lightTextTheme.bodyText2
          .merge(TextStyle(color: CMColors.backgroundBlack)),
    ),
    bottomAppBarTheme:
        BottomAppBarTheme(color: CMColors.backgroundWhite, elevation: 2),
    // bottomSheetTheme: BottomSheetThemeData(
    //   backgroundColor: Colors.black.withOpacity(0),
    //   modalBackgroundColor: Colors.transparent,
    //   elevation: 0,
    //   modalElevation: 0,
    //   clipBehavior: Clip.hardEdge,
    // ),
    tabBarTheme: TabBarTheme(
      unselectedLabelColor: CMColors.backgroundBlack,
      labelColor: CMColors.primaryPurple,
      indicatorSize: TabBarIndicatorSize.label,
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(color: CMColors.primaryPurple, width: 2.0),
      ),
    ),
    sliderTheme: SliderThemeData(
      activeTrackColor: CMColors.primaryPurple,
      inactiveTrackColor: CMColors.primaryPurple.withAlpha(140),
      trackShape: RoundedRectSliderTrackShape(),
      trackHeight: 4.0,
      thumbColor: CMColors.primaryPurple,
      thumbShape: RoundSliderThumbShape(enabledThumbRadius: 10.0),
      overlayShape: RoundSliderOverlayShape(overlayRadius: 24.0),
      tickMarkShape: RoundSliderTickMarkShape(),
      inactiveTickMarkColor: Colors.red[100],
      valueIndicatorShape: PaddleSliderValueIndicatorShape(),
      valueIndicatorTextStyle: TextStyle(
        color: Colors.white,
      ),
    ),
  );

  static getTheme() {
    return lightTheme;
  }
}
