part of '../cm_components.dart';

class CMSplitter extends StatelessWidget {
  final Text text;
  final double height;
  CMSplitter({@required this.text, this.height = 3}) : assert(text != null);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 11.0),
          child: text,
        ),
        Expanded(
          child: Container(
            color: CMColors.separatorBackground,
            height: height,
          ),
        )
      ],
    );
  }
}
