part of '../cm_components.dart';

class CMOperationIcon extends StatelessWidget {
  final Color backgroundColor;
  final Color shadowColor;
  final IconData icon;
  final double iconSize;
  final bool isAccepted;
  CMOperationIcon({
    @required this.icon,
    @required this.backgroundColor,
    @required this.shadowColor,
    @required this.isAccepted,
    this.iconSize,
  })  : assert(icon != null),
        assert(shadowColor != null),
        assert(shadowColor != null),
        assert(isAccepted != null);
  //TODO lополнить под стейкинг и другие иконки
  factory CMOperationIcon.fromData({
    @required String direction,
    @required String type,
    @required bool isAccepted,
  }) {
    if (type == 'transfer') {
      if (direction == 'income')
        return CMOperationIcon.receive(
          isAccepted: isAccepted,
        );
      else
        return CMOperationIcon.send(
          isAccepted: isAccepted,
        );
    } else if (type == 'reward') {
      return CMOperationIcon.reward(
        isAccepted: isAccepted,
      );
    } else if (type == 'stake') {
      return CMOperationIcon.stacking(
        isAccepted: isAccepted,
      );
    } else if (type == 'unstake') {
      return CMOperationIcon.unstacking(
        isAccepted: isAccepted,
      );
    } else if (type == 'vote') {
      return CMOperationIcon.voting(
        isAccepted: isAccepted,
      );
    } else if (type == 'delegation') {
      return CMOperationIcon.stacking(
        isAccepted: isAccepted,
      );
    } else if (type == 'fees') {
      return CMOperationIcon.fees(
        isAccepted: isAccepted,
      );
    } else
      return CMOperationIcon.unknown();
  }
  //TODO проверить размер
  CMOperationIcon.receive({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.receiveOperationIconBackground,
        shadowColor = CMColors.receiveOperationIconShadow,
        iconSize = 25,
        this.isAccepted = isAccepted,
        icon = CMIcons.receive;
  CMOperationIcon.send({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.sendOperationIconBackground,
        shadowColor = CMColors.sendOperationIconShadow,
        iconSize = 25,
        this.isAccepted = isAccepted,
        icon = CMIcons.send;
  CMOperationIcon.reward({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.rewardOperationIconBackground,
        shadowColor = CMColors.rewardOperationIconShadow,
        iconSize = 19,
        this.isAccepted = isAccepted,
        icon = CMIcons.reward;
  CMOperationIcon.stacking({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.stackingOperationIconBackground,
        shadowColor = CMColors.stackingOperationIconShadow,
        iconSize = 17,
        this.isAccepted = isAccepted,
        icon = CMIcons.stacking;
  CMOperationIcon.unstacking({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.unstackingOperationIconBackground,
        shadowColor = CMColors.unstackingOperationIconShadow,
        iconSize = 17,
        this.isAccepted = isAccepted,
        icon = CMIcons.unstacking;
  CMOperationIcon.voting({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.votingOperationIconBackground,
        shadowColor = CMColors.votingOperationIconShadow,
        iconSize = 23,
        this.isAccepted = isAccepted,
        icon = CMIcons.voting;
  CMOperationIcon.fees({VoidCallback onPressed, bool isAccepted})
      : backgroundColor = CMColors.feesOperationIconBackground,
        shadowColor = CMColors.feesOperationIconShadow,
        iconSize = 21,
        this.isAccepted = isAccepted,
        icon = CMIcons.fee;
  CMOperationIcon.unknown({VoidCallback onPressed})
      : backgroundColor = CMColors.feesOperationIconBackground,
        shadowColor = CMColors.feesOperationIconShadow,
        iconSize = 21,
        this.isAccepted = true,
        icon = Icons.sync_disabled_rounded;

  @override
  Widget build(BuildContext context) {
    //TODO доделать крестик в правом нижнем углу
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
              color: backgroundColor,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                    color: shadowColor, blurRadius: 10, offset: Offset(0, 4))
              ]),
          child: Icon(
            icon,
            size: iconSize,
            color: CMColors.backgroundWhite,
          ),
        ),
        if (!isAccepted)
          Positioned(
            bottom: 4,
            right: 0,
            //TODO проверить правильный ли отступ
            child: Transform.translate(
                offset: Offset(2, 0), child: _CMCrossError()),
          )
      ],
    );
  }
}

class _CMCrossError extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment(0.2, 0),
      width: 17,
      height: 17,
      decoration: BoxDecoration(
          color: CMColors.error,
          shape: BoxShape.circle,
          border: Border.all(width: 2, color: CMColors.backgroundWhite)),
      child: Icon(
        FontAwesomeIcons.times,
        size: 9,
        color: CMColors.backgroundWhite,
      ),
    );
  }
}
