part of '../cm_components.dart';

class CMActionSelector extends StatelessWidget {
  final List<Widget> children;
  final EdgeInsets pading;
  CMActionSelector(
      {@required this.children,
      this.pading = const EdgeInsets.symmetric(horizontal: 16)})
      : assert(children != null);
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: pading.copyWith(top: 20),
        decoration: BoxDecoration(
            color: CMColors.backgroundWhite,
            boxShadow: [
              BoxShadow(
                  offset: Offset.zero,
                  blurRadius: 15,
                  color: CMColors.actionSelectorShadow)
            ],
            borderRadius: BorderRadius.all(Radius.circular(16))),
        child: Column(
          children: <Widget>[
            ...ListTile.divideTiles(
                    color: CMColors.backgroundBlackLite,
                    context: context,
                    tiles: children)
                .toList(),
          ],
        ));
  }
}

class CMActionSelectorItem extends StatelessWidget {
  final Widget child;
  final onPressed;
  CMActionSelectorItem({@required this.child, @required this.onPressed})
      : assert(child != null),
        assert(onPressed != null);
  @override
  Widget build(BuildContext context) {
    return TextButton(
        style: ButtonStyle(
            padding: MaterialStateProperty.resolveWith<EdgeInsetsGeometry>(
          (Set<MaterialState> states) => EdgeInsets.zero,
        )),
        onPressed: onPressed,
        child: Container(
          child: child,
          alignment: Alignment.center,
          height: 58,
        ));
  }
}

class CMActionSelectorItemWithInfo extends StatelessWidget {
  final Widget header;
  final Widget child;
  final Function onPressed;
  CMActionSelectorItemWithInfo({this.child, this.header, this.onPressed});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: header,
          alignment: Alignment.center,
          height: 58,
          decoration: BoxDecoration(
            border: Border(
                bottom:
                    BorderSide(width: 0.5, color: CMColors.underlineBorder)),
          ),
        ),
        CupertinoButton(
          padding: EdgeInsets.zero,
          onPressed: onPressed,
          child: Container(
            child: child,
            alignment: Alignment.center,
            height: 58,
          ),
        ),
      ],
    );
  }
}
