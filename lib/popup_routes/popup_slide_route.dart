part of '../cm_components.dart';

class PopupSlideRoute extends PopupRoute {
  PopupSlideRoute({
    this.barrierLabel,
    @required this.parentData,
    @required this.children,
    @required this.animationDuration,
    this.asWideAsParent = true,
  })  : assert(children != null),
        assert(animationDuration != null),
        assert(parentData != null);
  final Rect parentData;
  final List<Widget> children;
  final Duration animationDuration;
  final bool asWideAsParent;
  @override
  Duration get transitionDuration => animationDuration;

  @override
  bool get barrierDismissible => true;

  @override
  Color get barrierColor => null;
  @override
  final String barrierLabel;

  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      removeBottom: true,
      removeLeft: true,
      removeRight: true,
      child: AnimatedBuilder(
        animation: animation,
        builder: (context, child) {
          return CustomSingleChildLayout(
            delegate: _PopupSlideRouteLayout(
                asWideAsParent: asWideAsParent,
                progress: animation.value,
                parentData: parentData),
            child: child,
          );
        },
        child: Material(
          color: Colors.transparent,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: children,
            ),
          ),
        ),
      ),
    );
  }
}

class _PopupSlideRouteLayout extends SingleChildLayoutDelegate {
  _PopupSlideRouteLayout(
      {@required this.progress,
      @required this.parentData,
      @required this.asWideAsParent})
      : assert(progress != null),
        assert(parentData != null),
        assert(asWideAsParent != null);
  final Rect parentData;
  final double progress;
  final bool asWideAsParent;

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    return BoxConstraints(
      minWidth: asWideAsParent ? parentData.width : constraints.minWidth,
      maxWidth: asWideAsParent ? parentData.width : constraints.maxWidth,
      minHeight: 0.0,
      maxHeight: constraints.maxHeight * 0.75,
    );
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    return Offset(asWideAsParent ? parentData.left : 0,
        size.height - childSize.height * progress);
  }

  @override
  bool shouldRelayout(_PopupSlideRouteLayout oldDelegate) {
    return progress != oldDelegate.progress;
  }
}
