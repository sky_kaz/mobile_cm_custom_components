part of '../cm_components.dart';

//отступ отверха в дизайне 79 из него вычитаем вертикальный margin уведомления 26
const double notificationTopOffset = 53;

class PopupNotificationRoute extends PopupRoute {
  PopupNotificationRoute({
    this.barrierLabel,
    @required this.notification,
    @required this.animationDuration,
  })  : assert(notification != null),
        assert(animationDuration != null);
  final Widget notification;
  final Duration animationDuration;
  @override
  Duration get transitionDuration => animationDuration;

  @override
  bool get barrierDismissible => true;

  @override
  Color get barrierColor => null;
  @override
  final String barrierLabel;

  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      removeBottom: true,
      removeLeft: true,
      removeRight: true,
      child: AnimatedBuilder(
        animation: animation,
        builder: (context, child) {
          return CustomSingleChildLayout(
            delegate: _PopupNotificationRouteLayout(
              progress: animation.value,
            ),
            child: child,
          );
        },
        child: Material(
          color: Colors.transparent,
          child: GestureDetector(child:notification, onTap: ()=> Navigator.of(context).maybePop(),),
        ),
      ),
    );
  }
}

class _PopupNotificationRouteLayout extends SingleChildLayoutDelegate {
  _PopupNotificationRouteLayout({
    @required this.progress,
  }) : assert(progress != null);
  final double progress;

  @override
  BoxConstraints getConstraintsForChild(BoxConstraints constraints) {
    return BoxConstraints(
      minWidth: constraints.minWidth,
      maxWidth: constraints.maxWidth,
      minHeight: 0.0,
      maxHeight: constraints.maxHeight * 0.4,
    );
  }

  @override
  Offset getPositionForChild(Size size, Size childSize) {
    return Offset(
        0,
        (childSize.height + notificationTopOffset) * progress -
            childSize.height);
  }

  @override
  bool shouldRelayout(_PopupNotificationRouteLayout oldDelegate) {
    return progress != oldDelegate.progress;
  }
}
