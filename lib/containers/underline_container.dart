part of '../cm_components.dart';

class CMUnderlineBox extends StatelessWidget {
  final EdgeInsets innerPadding;
  final Widget child;
  CMUnderlineBox({this.innerPadding, this.child});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: innerPadding,
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(width: 1, color: CMColors.underlineBorder))),
      child: child,
    );
  }
}
