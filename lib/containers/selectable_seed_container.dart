part of '../cm_components.dart';

class CMSelectableSeedContainer extends StatelessWidget {
  final List<String> selectedSeedList;
  final ValueChanged onSeedTap;
  final String text;
  CMSelectableSeedContainer({this.selectedSeedList, @required this.onSeedTap,this.text,})
      : assert(onSeedTap != null);
  final Duration animationDuration = Duration(milliseconds: 250);
  @override
  Widget build(BuildContext context) {
    return DottedBorder(
        dashPattern: const <double>[2, 2],
        borderType: BorderType.RRect,
        radius: Radius.circular(10),
        padding: EdgeInsets.zero,
        color: CMColors.dashedBorder,
        child: Container(
          alignment: Alignment.center,
          constraints: BoxConstraints.expand(height: 255),
          child: AnimatedSwitcher(
            duration: animationDuration,
            child: (selectedSeedList?.isNotEmpty ?? false)
                ? _buildSeedTable(selectedSeedList)
                : Padding(
                    padding: EdgeInsets.all(52),
                    child: Text(
                   text,
                      textAlign: TextAlign.center,
                      style: CMTextStyle.selectableSeedContainerText,
                    ),
                  ),
          ),
        ));
  }

  Widget _buildSeedTable(List<String> seedList) {
    List<Widget> seeds = [];
    for (int i = 0; i < 12; i++) {
      if (i < seedList.length) {
        seeds.add(AnimatedSwitcher(
          duration: animationDuration,
          transitionBuilder: (child, animation) {
            return ScaleTransition(
              alignment: Alignment.center,
              scale: animation,
              child: child,
            );
          },
          child: CMSeedButton(
              onPressed: () => onSeedTap(seedList[i]),
              key: ValueKey<String>(seedList[i]),
              text: seedList[i]),
        ));
      } else {
        seeds.add(AnimatedSwitcher(
          duration: animationDuration,
          transitionBuilder: (child, animation) {
            return ScaleTransition(
              alignment: Alignment.center,
              scale: animation,
              child: child,
            );
          },
          child: Container(key: ValueKey<int>(i), width: 100, height: 40),
        ));
      }
    }
    return CMSeedTable(
      seeds: seeds,
    );
  }
}
