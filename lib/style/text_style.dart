part of '../cm_components.dart';

class CMTextStyle {
  static const title = TextStyle(
      height: 1.2,
      fontSize: 23,
      fontWeight: FontWeight.bold,
      color: CMColors.title);
  static const heading1 = TextStyle(
      fontSize: 30,
      fontWeight: FontWeight.bold,
      color: CMColors.backgroundBlack);
  static const heading2 =
      TextStyle(fontSize: 17, letterSpacing: -0.02, color: CMColors.heading2);
  static const normal1 =
      TextStyle(fontSize: 14, color: CMColors.backgroundBlack);
  static const normal2 =
      TextStyle(fontSize: 12, color: CMColors.normal, height: 1.66);
  //TODO изменить название
  static const black14pxText =
      TextStyle(color: CMColors.backgroundBlack, fontSize: 14);

  //TEXT FIELD
  static const textFieldHintStyle = const TextStyle(
      fontSize: 17,
      letterSpacing: -0.02,
      color: CMColors.navigationBarIconUnselected);
  static const textFieldLabelStyle =
      const TextStyle(color: CMColors.backgroundPurpleLite, fontSize: 14);
  static const textFieldHintStyleDisabled = const TextStyle(
      fontSize: 17,
      letterSpacing: -0.02,
      color: Color.fromRGBO(214, 214, 214, 1));
  static const textFieldLabelStyleDisabled =
      const TextStyle(color: Color.fromRGBO(214, 214, 214, 1), fontSize: 14);
  //SEPARATOR
  static const textFieldSeparator = const TextStyle(
      fontSize: 17.0,
      letterSpacing: -0.02,
      color: CMColors.backgroundBlack,
      fontWeight: FontWeight.w500);

  //EXPANDED TEXT
  static const expandedTextLeftStyle = TextStyle(
    fontSize: 12,
    color: CMColors.expandedTextLeft,
    letterSpacing: -0.02,
  );
  static const expandedTextRightStyle = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: CMColors.expandedTextRight,
    letterSpacing: -0.02,
  );
  //NODE LIST TILE
  static const nodeListTileTitleStyle = TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 17,
  );
  //OPERATION LIST TILE
  //TODO название
  static const amountTextStyle = TextStyle(
      letterSpacing: -0.02,
      height: 1.21,
      fontSize: 14,
      fontWeight: FontWeight.w600,
      color: CMColors.primaryPurpleLite);

  //REWARD LIST TILE
  static const rewardListTileAmountTextStyle = TextStyle(
      fontSize: 18,
      height: 1.22,
      fontWeight: FontWeight.bold,
      color: CMColors.backgroundBlack);
  static const rewardListTileRewardsTitleTextStyle = TextStyle(
      fontWeight: FontWeight.bold,
      color: CMColors.backgroundPurpleLite,
      fontSize: 17,
      height: 1.23);
  //SCORE TITLE
  static const scoreTitleSubtitleTextStyle = TextStyle(
      fontSize: 14, color: CMColors.purpleAccent, fontWeight: FontWeight.bold);
  //TAB BAR
  static const tabBarLabelStyle = TextStyle(
      fontSize: 12.5, fontWeight: FontWeight.w600, letterSpacing: 0.1);
  static const tabBarUnselectedLabelStyle = TextStyle(
      fontSize: 12.5, fontWeight: FontWeight.w600, letterSpacing: 0.1);
  //FLAT BUTTON
  static const flatButtonText = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w600,
      letterSpacing: -0.02,
      color: CMColors.backgroundWhite);
  //RAISED BUTTON
  static const raisedButtonText = TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w600,
      letterSpacing: -0.02,
      color: CMColors.backgroundWhite);
  //WALLET LIST TILE
  static const walletListTileTitleSelected = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w600,
      height: 1.25,
      letterSpacing: -0.02,
      color: CMColors.backgroundWhite);
  static const walletListTileTitleUnselected = TextStyle(
      fontSize: 16,
      height: 1.25,
      fontWeight: FontWeight.w600,
      letterSpacing: -0.02,
      color: CMColors.backgroundBlack);
  static const walletListTileSubtitleSelected = TextStyle(
      fontSize: 14,
      height: 1.21,
      fontWeight: FontWeight.w300,
      color: CMColors.backgroundWhite);
  static const walletListTileSubtitleUnselected = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w300,
      height: 1.21,
      color: CMColors.purpleAccent);
  //ADD AN ADDRESS TILE
  //title
  static const addAnAddressTileTitleSelected = TextStyle(
      fontFamily: 'Montserrat',
      fontSize: 16,
      fontWeight: FontWeight.w700,
      height: 1.21,
      letterSpacing: -0.02,
      color: CMColors.backgroundWhite);
  static const addAnAddressTileTitleUnselected = TextStyle(
      fontFamily: 'Montserrat',
      fontSize: 16,
      height: 1.21,
      fontWeight: FontWeight.w700,
      letterSpacing: -0.02,
      color: CMColors.backgroundBlack);
  //subtitle
  static const addAnAddressTileSubtitleSelected = TextStyle(
      fontFamily: 'Montserrat',
      fontSize: 14,
      height: 1.21,
      fontWeight: FontWeight.w300,
      color: CMColors.backgroundWhite);
  static const addAnAddressTileSubtitleUnselected = TextStyle(
      fontFamily: 'Montserrat',
      fontSize: 14,
      fontWeight: FontWeight.w300,
      height: 1.21,
      color: CMColors.purpleAccent);
  //note
  static const addAnAddressTileNoteSelected = TextStyle(
      fontFamily: 'Montserrat',
      fontStyle: FontStyle.italic,
      fontSize: 10,
      fontWeight: FontWeight.w300,
      height: 1.21,
      color: CMColors.backgroundWhite);
  static const addAnAddressTileNoteUnselected = TextStyle(
      fontFamily: 'Montserrat',
      fontStyle: FontStyle.italic,
      fontSize: 10,
      fontWeight: FontWeight.w300,
      height: 1.21,
      color: CMColors.purpleAccent);
  //recommendtitle
  static const addAnAddressTileRecommendTitleSelected = TextStyle(
      fontFamily: 'Montserrat',
      fontSize: 14,
      fontWeight: FontWeight.w700,
      height: 1.22,
      color: CMColors.backgroundWhite);
  static addAnAddressTileRecommendTitleUnselected(Color myColor) {
    return TextStyle(
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontWeight: FontWeight.w700,
        height: 1.21,
        color: myColor);
  }

//IMPORT ADDRESS TILE
  static const importAddressTileTitleSelected = TextStyle(
      fontSize: 20,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w700,
      height: 1.21,
      color: CMColors.backgroundWhite);
  static const importAddressTileTitleUnselected = TextStyle(
      fontSize: 20,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w700,
      height: 1.21,
      color: CMColors.importAddressTileTitleUnselected);
  static const importAddressTileSubtitleSelected = TextStyle(
      fontSize: 14,
      height: 1.21,
      fontWeight: FontWeight.w400,
      color: CMColors.backgroundWhite);
  static const importAddressTileSubtitleUnselected = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      height: 1.21,
      color: CMColors.purpleAccent);

  static const adderessSubtitleUnselected = TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w400,
      height: 1.21,
      color: CMColors.backgroundBlack);

  //CHECKBOX TILE
  static const checkboxTileCurrency = TextStyle(
      fontSize: 21,
      fontWeight: FontWeight.w600,
      color: CMColors.backgroundBlack,
      letterSpacing: -0.02);
  //SEED CONTAINER
  static const seedContainerText = TextStyle(
      color: CMColors.seedContainerText,
      fontSize: 14,
      fontWeight: FontWeight.w500);
//SELECTABLE SEED CONTAINER
  static const selectableSeedContainerText = TextStyle(
      color: CMColors.selectableSeedContainerText, fontSize: 14, height: 1.42);
  //SWITCH TILE
  static const switchTileText = TextStyle(
      color: CMColors.backgroundBlack,
      fontSize: 17,
      height: 1.29,
      fontWeight: FontWeight.w300);
//ACTION SELECTOR
  static const actionSelectorText = TextStyle(
      color: CMColors.actionSelectorText,
      fontSize: 20,
      letterSpacing: 0.0375,
      fontWeight: FontWeight.bold);
//ACTION SELECTOR ITEM
  static const actionSelectorItemText = TextStyle(
      fontSize: 18, letterSpacing: 0.019, color: CMColors.actionSelectorText);
  //APPBAR
  static const appBarTitle = TextStyle(
      color: CMColors.backgroundPurpleLite,
      fontSize: 17,
      height: 1.64,
      fontWeight: FontWeight.w600);
  //SHARE BUTTON
  static const shareButtonText = TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w500,
      color: CMColors.shareButtonBackground,
      height: 1.66);
  //CURRENCY TILE
  static const currencyTileTitle = TextStyle(
      fontSize: 19,
      height: 1.21,
      fontWeight: FontWeight.w600,
      color: CMColors.backgroundBlack,
      letterSpacing: -0.02);
  static const currencyTileSubtitle = TextStyle(
      fontSize: 17,
      height: 1.23,
      fontWeight: FontWeight.w600,
      letterSpacing: -0.02,
      color: CMColors.primaryPurple);
  //REWARD NOTIFICATION BUTTON
  static const rewardNotificationButton = TextStyle(
      fontSize: 14,
      height: 1.21,
      fontWeight: FontWeight.w600,
      letterSpacing: -0.02,
      color: CMColors.backgroundWhite);

  static const titleThinTextStyle = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w400, color: CMColors.heading2);


  //QR Code Pages
  static const qrCodeInfoPageTitle = TextStyle(
      fontSize: 25,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w700,
      height: 1.21,
      color: CMColors.primaryPurple
  );

  static const qrCodeInfoPageRowNumbers = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w700,
      height: 1.20,
      color: CMColors.primaryPurple
  );

  static const qrCodeInfoPageRowText = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.20,
      color: Colors.black
  );

  static const qrCodeCameraIsDisabledTitle = TextStyle(
      fontSize: 18,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.25,
      color: Color.fromRGBO(136, 136, 136, 1)
  );

  static const qrCodeCameraInfoText = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.24,
      color: Colors.black
  );

  static const qrCodeCameraOpenQrInfo = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.17,
      color: Colors.white
  );

  static const qrCodeCameraErrorTitle = TextStyle(
      fontSize: 16,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      height: 1.19,
      color: Colors.white
  );
  static const qrCodeCameraErrorDesc = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.17,
      color: Colors.white
  );

  static const qrPasswordPageTitle = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.17,
      color: Colors.black
  );

  static const importedAddressItemNetTitle = TextStyle(
      fontSize: 17,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      height: 1.17,
      color: Colors.black
  );
  static const importedAddressItemWallet = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.30,
      color: Colors.black
  );
  static const notFoundNet = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.30,
      color: Colors.red
  );
  static const importedAddressItemNameOfNet = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w300,
      height: 1.17,
      color: Colors.black
  );

  static const importedAddressItemValueOfWallet = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      height: 1.17,
      color: Color.fromRGBO(160, 32, 255, 1)
  );
  static const listTitleText = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w400,
      height: 1.30,
      color: CMColors.title);
  static const privateKey = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w500,
      color: CMColors.title);

  static const deleteText = TextStyle(
    color: Color.fromRGBO(225, 66, 66, 1),
    fontWeight: FontWeight.w700,
    fontSize: 18,
  );

  static const deleteTheSelectedText = TextStyle(
    color: Color.fromRGBO(136, 136, 136, 1),
    fontWeight: FontWeight.w400,
    fontSize: 14,
  );

  static const publicAddressText = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w500,
      color: Colors.black);

  static const privateKeyText = TextStyle(
    color: CMColors.scoreTileText,
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w500,
    fontSize: 12,
  );

  static const privateAddressText = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w500,
      color: CMColors.title);

  static const copyText = TextStyle(
    color: CMColors.primaryPurple,
    fontWeight: FontWeight.w400,
    fontSize: 18,
  );

  static const exportPhrase = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontSize: 17,
  );

  static const exportMnemonicPhrase = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      color: Colors.white);

  static const exportButtonText = TextStyle(
      fontSize: 14,
      fontFamily: 'Montserrat',
      fontWeight: FontWeight.w600,
      color: CMColors.purpleAccent
  );
}
