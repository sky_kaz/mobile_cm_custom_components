part of '../cm_components.dart';

Future<T> showSelectorBottomSheet<T>(BuildContext context,
    {double maxChildSize = 1.0,
    double initialChildSize = 0.4,
    double minChildSize = 0.25,
    String text,
    @required List<Widget> children}) {
  assert(maxChildSize != null);
  assert(children != null);
  return showModalBottomSheet<T>(
      isScrollControlled: true,
      context: context,
      barrierColor: Colors.transparent,
      builder: (context) {
        return Stack(children: [
          GestureDetector(
            onTap: () => Navigator.pop(context),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: DraggableScrollableSheet(
                expand: false,
                minChildSize: minChildSize,
                initialChildSize: initialChildSize,
                maxChildSize: maxChildSize,
                builder: (BuildContext context, ScrollController controller) {
                  return Column(
                    children: [
                      Expanded(
                        child: Container(
                            clipBehavior: Clip.antiAlias,
                            margin: EdgeInsets.symmetric(horizontal: 16),
                            decoration: BoxDecoration(
                                color: CMColors.backgroundWhite,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black.withOpacity(0.1),
                                      blurRadius: 15)
                                ],
                                borderRadius: BorderRadius.circular(14)),
                            child: Scrollbar(
                              child: ListView(
                                padding: EdgeInsets.symmetric(vertical: 4),
                                physics: ClampingScrollPhysics(),
                                controller: controller,
                                children: children,
                              ),
                            )),
                      ),
                      CMActionSelectorCloseButton(
                        text: text,
                      )
                    ],
                  );
                }),
          )
        ]);
      });
}
