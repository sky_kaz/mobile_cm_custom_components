part of '../cm_components.dart';

class CMTapUnfocus extends StatelessWidget {
  final Widget child;
  CMTapUnfocus({@required this.child}) : assert(child != null);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          if (FocusScope.of(context).focusedChild != null) {
            FocusScope.of(context).focusedChild.unfocus();
          }
        },
        child: child);
  }
}
