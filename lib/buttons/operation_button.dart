part of '../cm_components.dart';

class CMOperationIconWidget extends StatelessWidget {
  final Color backgroundColor;
  final Color shadowColor;
  final IconData icon;
  final double iconSize;
  final VoidCallback onPressed;
  CMOperationIconWidget(
      {@required this.icon,
      @required this.backgroundColor,
      @required this.shadowColor,
      this.iconSize,
      this.onPressed})
      : assert(icon != null),
        assert(shadowColor != null),
        assert(shadowColor != null);
  //TODO проверить размер
  CMOperationIconWidget.receive(VoidCallback onPressed)
      : backgroundColor = CMColors.receiveOperationIconBackground,
        shadowColor = CMColors.receiveOperationIconShadow,
        iconSize = 25,
        icon = CMIcons.receive,
        this.onPressed = onPressed;
  CMOperationIconWidget.send(VoidCallback onPressed)
      : backgroundColor = CMColors.sendOperationIconBackground,
        shadowColor = CMColors.sendOperationIconShadow,
        iconSize = 25,
        icon = CMIcons.send,
        this.onPressed = onPressed;
  CMOperationIconWidget.reward(VoidCallback onPressed)
      : backgroundColor = CMColors.rewardOperationIconBackground,
        shadowColor = CMColors.rewardOperationIconShadow,
        iconSize = 19,
        icon = CMIcons.reward,
        this.onPressed = onPressed;
  CMOperationIconWidget.stacking(VoidCallback onPressed)
      : backgroundColor = CMColors.stackingOperationIconBackground,
        shadowColor = CMColors.stackingOperationIconShadow,
        iconSize = 17,
        icon = CMIcons.stacking,
        this.onPressed = onPressed;
  CMOperationIconWidget.unstacking(VoidCallback onPressed)
      : backgroundColor = CMColors.unstackingOperationIconBackground,
        shadowColor = CMColors.unstackingOperationIconShadow,
        iconSize = 17,
        icon = CMIcons.unstacking,
        this.onPressed = onPressed;
  CMOperationIconWidget.voting(VoidCallback onPressed)
      : backgroundColor = CMColors.votingOperationIconBackground,
        shadowColor = CMColors.votingOperationIconShadow,
        iconSize = 23,
        icon = CMIcons.voting,
        this.onPressed = onPressed;

  bool get enabled => onPressed != null;
  @override
  Widget build(BuildContext context) {
    //TODO доделать крестик в правом нижнем углу
    return Semantics(
      container: true,
      button: true,
      enabled: enabled,
      child: Container(
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            color: backgroundColor,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                  color: shadowColor, blurRadius: 10, offset: Offset(0, 4))
            ]),
        child: Material(
          color: Colors.transparent,
          type: MaterialType.circle,
          child: InkWell(
            onTap: onPressed,
            customBorder: CircleBorder(),
            child: Icon(
              icon,
              size: iconSize,
              color: CMColors.backgroundWhite,
            ),
          ),
        ),
      ),
    );
  }
}
