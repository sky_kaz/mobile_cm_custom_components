import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SettingsAddButton extends StatelessWidget {
  final VoidCallback onPressed;

  const SettingsAddButton({this.onPressed});
  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.zero,
      child: SvgPicture.asset("cm_components/assets/plus.svg"),
      onPressed: onPressed,
    );
  }
}
