part of '../cm_components.dart';

class CMIconRaisedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String icon;
  final Color color;
  final Color iconColor;
  final Color shadowColor;

  CMIconRaisedButton({
    this.onPressed,
    @required this.icon,
    @required this.color,
    this.iconColor = CMColors.backgroundWhite,
    this.shadowColor = Colors.black12,
  }) : assert(icon != null);

  @override
  Widget build(BuildContext context) {
    final button = FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      color: color,
      onPressed: onPressed,
      child: SvgPicture.asset(
        icon,
        color: iconColor,
      ),
    );

    return Container(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 5),
            blurRadius: 7,
            color: shadowColor,
          )
        ],
      ),
      width: 50,
      height: 50,
      child: button,
    );
  }
}
