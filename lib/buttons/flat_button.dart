part of '../cm_components.dart';

class CMFlatButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  CMFlatButton({this.onPressed, @required this.text}) : assert(text != null);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: CMColors.flatButtonBackground,
          borderRadius: BorderRadius.all(Radius.circular(50)),
          boxShadow: [
            BoxShadow(
              offset: Offset.zero,
              blurRadius: 25,
              color: CMColors.flatButtonShadow,
            )
          ]),
      width: 230,
      height: 60,
      child: FlatButton(
        disabledColor: CMColors.flatButtonDisabledColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50))),
        color: Colors.transparent,
        onPressed: onPressed,
        child: Text(
          text,
          style: CMTextStyle.flatButtonText,
        ),
      ),
    );
  }
}

class CMFlatButtonDisabled extends StatelessWidget {
  final String text;
  CMFlatButtonDisabled({@required this.text}) : assert(text != null);
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: CMColors.flatButtonDisabledColor,
        borderRadius: BorderRadius.all(Radius.circular(50)),
      ),
      width: 230,
      height: 60,
      child: Text(
        text,
        style: CMTextStyle.flatButtonText
            .copyWith(color: CMColors.checkboxUnselected),
        textAlign: TextAlign.center,
      ),
    );
  }
}
