import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SettingsBackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: CupertinoButton(
        //color: Colors.green,
        padding: EdgeInsets.zero,
        child: SvgPicture.asset("cm_components/assets/back.svg"),
        onPressed: () => Navigator.maybePop(context),
      ),
    );
  }
}
