part of '../cm_components.dart';

class CMSeedButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  CMSeedButton({Key key, @required this.text, this.onPressed})
      : assert(text != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(width: 100, height: 40),
      child: FlatButton(
        color: CMColors.seedContainerBackground,
        disabledColor: CMColors.seedContainerBackground,
        padding: EdgeInsets.all(6),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
        onPressed: onPressed,
        child: Center(
          child: Text(
            text,
            style: CMTextStyle.seedContainerText.copyWith(
                fontSize: _fontSize(MediaQuery.of(context).size.width)),
          ),
        ),
      ),
    );
  }

  double _fontSize(double screenWidth) {
    if (screenWidth < 360)
      return 8;
    else
      return 11;
  }
}

class CMSeedButtonBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DottedBorder(
        dashPattern: const <double>[2, 2],
        borderType: BorderType.RRect,
        radius: Radius.circular(50),
        padding: EdgeInsets.zero,
        color: CMColors.dashedBorder,
        child: Container(
          width: 99,
          height: 39,
        ));
  }
}
