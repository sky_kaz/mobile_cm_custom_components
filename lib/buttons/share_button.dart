part of '../cm_components.dart';

class CMShareButton extends StatelessWidget {
  final VoidCallback onPressed;
  CMShareButton({this.onPressed});

  bool get enabled => onPressed != null;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
            padding: EdgeInsets.only(right: 16),
            child: Semantics(
              container: true,
              button: true,
              enabled: enabled,
              child: Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    color: CMColors.shareButtonBackground,
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                          color: CMColors.shareButtonShadow,
                          blurRadius: 10,
                          offset: Offset(0, 4))
                    ]),
                child: Material(
                  color: Colors.transparent,
                  type: MaterialType.circle,
                  child: InkWell(
                    onTap: onPressed,
                    customBorder: CircleBorder(),
                    child: Icon(
                      CMIcons.share,
                      size: 19,
                      color: CMColors.backgroundWhite,
                    ),
                  ),
                ),
              ),
            )),
        Text(
          "Share",
          style: CMTextStyle.shareButtonText,
        )
      ],
    );
  }
}
