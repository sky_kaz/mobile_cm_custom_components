part of '../cm_components.dart';

class CMRewardNotificationButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final Color backgroundColor;
  final Color textColor;

  CMRewardNotificationButton(
      {this.onPressed,
      @required this.text,
      @required this.backgroundColor,
      @required this.textColor})
      : assert(text != null),
        assert(backgroundColor != null),
        assert(textColor != null);
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.tightFor(width: 80, height: 30),
      child: FlatButton(
        disabledColor: backgroundColor,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50))),
        color: backgroundColor,
        onPressed: onPressed,
        child: Text(
          text,
          style: CMTextStyle.rewardNotificationButton,
        ),
      ),
    );
  }
}
