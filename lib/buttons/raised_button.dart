part of '../cm_components.dart';

class CMRaisedButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final Color color;
  final Color shadowColor;

  CMRaisedButton({
    this.onPressed,
    @required this.text,
    this.color = CMColors.raisedButtonBackground,
    this.shadowColor = CMColors.raisedButtonShadowOrange,
  }) : assert(text != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(50)),
          boxShadow: [
            BoxShadow(
              offset: Offset.zero,
              blurRadius: 25,
              color: shadowColor,
            )
          ]),
      width: 200,
      height: 60,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(50))),
        color: Colors.transparent,
        elevation: 0,
        highlightElevation: 0,
        disabledElevation: 0,
        focusElevation: 0,
        hoverElevation: 0,
        onPressed: onPressed,
        child: Text(text,
            style: CMTextStyle.raisedButtonText, textAlign: TextAlign.center),
      ),
    );
  }
}
