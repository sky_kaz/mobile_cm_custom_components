part of '../cm_components.dart';

class CMActionSelectorCloseButton extends StatelessWidget {
  final EdgeInsets padding;
  final String text;
  CMActionSelectorCloseButton({
    this.padding = const EdgeInsets.symmetric(horizontal: 16),
    @required this.text,
  });
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        constraints: BoxConstraints.expand(height: 57),
        child: Padding(
          padding: padding,
          child: FlatButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(16))),
            color: CMColors.actionSelectorCancelButton,
            child: Text(
              text,
              style: CMTextStyle.actionSelectorText,
            ),
            //TODO закрытие при нажатии кнопки
            //todo: [RD] change from 'Navigator.of(context).pop' to 'Navigator.maybePop(context)'
            onPressed: () => Navigator.maybePop(context),
          ),
        ));
  }
}
