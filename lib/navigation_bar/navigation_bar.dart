part of '../cm_components.dart';

class CMNavigationBar extends StatelessWidget {
  final int currentIndex;
  final ValueChanged<int> onTap;
  CMNavigationBar({this.currentIndex, this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: CMColors.navigationBarBackground,
          borderRadius: BorderRadius.all(Radius.circular(50)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: CMColors.navigationBarShadow,
              blurRadius: 25,
            )
          ]),
      height: 60,
      margin: EdgeInsets.only(
        bottom: 20,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <CMNavigationBarIconButton>[
          CMNavigationBarIconButton(
            buttonSize: 60,
            onPressed: () => onTap(0),
            icon: CMIcons.wallet,
            iconSize: 37,
            iconColor: switchColor(0, currentIndex),
          ),
          CMNavigationBarIconButton(
            buttonSize: 60,
            onPressed: () => onTap(1),
            icon: CMIcons.discount,
            iconSize: 30,
            iconColor: switchColor(1, currentIndex),
          ),
          CMNavigationBarIconButton(
            buttonSize: 60,
            onPressed: () => onTap(2),
            icon: CMIcons.transfer,
            iconSize: 32,
            iconColor: switchColor(2, currentIndex),
          ),
          CMNavigationBarIconButton(
            buttonSize: 60,
            onPressed: () => onTap(3),
            icon: CMIcons.settings,
            iconSize: 28,
            iconColor: switchColor(3, currentIndex),
          ),
        ],
      ),
    );
  }

  Color switchColor(int index, int currentIndex) {
    if (currentIndex == index)
      return CMColors.backgroundWhite;
    else
      return CMColors.navigationBarIconUnselected;
  }
}
