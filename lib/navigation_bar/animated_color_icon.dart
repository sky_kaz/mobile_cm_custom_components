part of '../cm_components.dart';

class AnimatedColorIcon extends ImplicitlyAnimatedWidget {
  AnimatedColorIcon({
    Key key,
    @required this.color,
    @required this.size,
    @required this.icon,
    Curve curve = Curves.linear,
    @required Duration duration,
    VoidCallback onEnd,
  })  : assert(color != null),
        assert(size != null),
        assert(icon != null),
        super(key: key, curve: curve, duration: duration, onEnd: onEnd);

  final IconData icon;
  final double size;
  final Color color;

  @override
  _AnimatedContainerState createState() => _AnimatedContainerState();
}

class _AnimatedContainerState
    extends AnimatedWidgetBaseState<AnimatedColorIcon> {
  ColorTween _colorTween;

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    _colorTween = visitor(_colorTween, widget.color,
        (dynamic value) => ColorTween(begin: value as Color)) as ColorTween;
  }

  @override
  Widget build(BuildContext context) {
    return Icon(
      widget.icon,
      size: widget.size,
      color: _colorTween.evaluate(animation),
    );
  }
}
