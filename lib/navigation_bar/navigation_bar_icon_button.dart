part of '../cm_components.dart';

class CMNavigationBarIconButton extends StatelessWidget {
  final VoidCallback onPressed;
  final double buttonSize;
  final IconData icon;
  final double iconSize;
  final Color iconColor;
  CMNavigationBarIconButton(
      {@required this.buttonSize,
      @required this.icon,
      @required this.iconColor,
      @required this.iconSize,
      this.onPressed})
      : assert(buttonSize != null),
        assert(icon != null),
        assert(iconColor != null),
        assert(iconSize != null);
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPressed,
      constraints:
          BoxConstraints.tightFor(height: buttonSize, width: buttonSize),
      padding: EdgeInsets.zero,
      shape: CircleBorder(),
      child: AnimatedColorIcon(
        icon: icon,
        size: iconSize,
        color: iconColor,
        duration: Duration(milliseconds: 400),
      ),
    );
  }
}
